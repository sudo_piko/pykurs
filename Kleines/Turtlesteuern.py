# Piko ist begeistert.
# 04.01.2022

import turtle


def dreieck(laenge=100):
    for i in range(3):
        turtle.fd(laenge)
        turtle.lt(120)


def vor():
    turtle.fd(10)

def rechts():
    turtle.rt(5)

def links():
    turtle.lt(5)



dreieck(100)

s = turtle.Screen()
s.listen()


s.onkeypress(dreieck, "d")

for a, b in [(vor, "v"), (rechts, "r"), (links, "l")]:
    s.onkeypress(a, b)

