# banana.py

from turtle import *

def schaelen(anzahl):
	print("Diese Frucht ist geschält!" * anzahl)

def dreieck(laenge):
	for i in range(3):
		fd(laenge)
		rt(120)

def jump(laenge):
	pu()
	fd(laenge)
	pd()


circle(200)



#
# # Strings ausführen lassen:
# lustige_variable = "circle(100)"
# exec(lustige_variable)
#
# # das macht dann das gleiche wie:
# circle(100)





# In der Variablen __name__ ist der String "__main__" drin, als hätte Python für uns folgendes ausgeführt:
# __name__ = "__main__"
# Das ist genau die gleiche Syntax wie:
# eine_zahl = 2

print("Banana: name ist gerade: ", __name__)

if __name__ == "__main__":
	schaelen(1)
	dreieck(100)
	jump(200)
	dreieck(100)

if __name__ == "banana":   # Wan wird das ausgeführt?
	print("eaiutduaierntaieuntdaieu")