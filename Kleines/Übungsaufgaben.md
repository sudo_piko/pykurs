Übungsaufgaben
===

Hier sammeln sich kleine Aufgaben, die sich so zwischendurch machen lassen. Sie sollten ungefähr nach Schwierigkeitsgrad sortiert sein.


Schreibe ein Programm, das mit einer for-Schleife die Zahlen von 1 bis 20 printet. Schreibe dann ein Programm, das das gleiche tut, aber eine while-Schleife verwendet.

