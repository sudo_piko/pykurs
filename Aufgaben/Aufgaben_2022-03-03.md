# Aufgaben am 


## 1 – Fahrradwerkstatt
Das hier ist der Code von Desiderat:
```python
import turtle 
####Code angepasst und erweitert:


class Fahrrad:
    
    # __init__ baut uns ein neues Fahrrad-Objekt:
    def __init__(self, besitzy,farbe, reifen_groesse, kaputt=False):
        self.farbe = farbe #Attribut
        self.reifen_groesse = reifen_groesse
        self.kaputt = kaputt
        self.besitzy = besitzy
    
    def kaputtmachen(self):
        self.kaputt = True
        
    def reparieren(self):
        self.kaputt = False
        
    def verschenken_an(self, beschenkte_person):
        self.besitzy = beschenkte_person
    
    def wert_zeigen(self):
        print(f"Das Fahrrad von {self.besitzy} ist {self.farbe} und hat die Reifengröße {self.reifen_groesse}")

    def malen(self):
        t = turtle.Turtle()
        t.pensize(2)
        t.pencolor(self.farbe)
        t.lt(120)
        # Hinterreifen
        t.pd()
        t.circle(self.reifen_groesse)
        t.pu()
        # Sattel und Stange
        t.lt(75)
        t.fd(10)
        t.lt(25)
        t.fd(self.reifen_groesse/2)
        t.pd()
        t.rt(45)
        t.fd(100)       
        # Vorderreifen
        t.pd()
        t.circle(self.reifen_groesse)
      
        #Stange und Lenker
        t.rt(75)
        t.fd(40)
        t.rt(70)
        t.fd(25)

# Das lässt sich dann so aufrufen:
fahrrad1 = Fahrrad("Ada", "blue",24)
fahrrad2 = Fahrrad("Margaret", "red",12)

#fahrrad1.wert_zeigen()
fahrrad1.malen()
#turtle.fd(200)
#fahrrad2.malen()


turtle.exitonclick()
```

- Verändert malen() so, dass das Fahrrad etwas anders aussieht.
- definiert drei neue Methoden, zum Beispiel: 
  - klingeln
  - klingeln mit sound-ausgabe
  - fahren (mit positionsattributen)
  - lenken
  - leuchten
  - Reifen aufpumpen
  - Anhänger
  - anmalen/bestickern

Für manche dieser Methoden müsst Ihr neue Attribute hinzufügen.


## 2 – Dreiecks-Klasse

Definiert eine Klasse namens Dreieck. Welche Attribute könnte denn ein Dreieck so haben?
Gebt ihr die beiden Methoden "malen" und "Werte ausgeben"