# Veränderung von einer Version einer Pythonista (Finja I think?)
# Piko 13.01. 22:00

from turtle import *

stichlaenge = 10

vokale = "aeiou"
konsonanten = "bcdfghjklmnpqrstvwxyz"


def gen_binary_list(satz):
    list = []
    for c in satz.lower():
        if c in vokale:
            list.append(1)
        elif c in konsonanten:
            list.append(0)
    return list


def stitch(start, number_lines, len_stitches=None, length=10):  # neu: number_stitches
    print(start)
    if not len_stitches:
        number_stiches = number_lines
    number_stitches = len_stitches//2
    if start == 0:
        penup()
        fd(length)
        pendown()
    for i in range(number_stitches):
        fd(length)
        penup()
        fd(length)
        pendown()
    if start != 0:
        pu()
        fd(length)
        pd()
    #
    #
    #
    #     n_l = number_lines - 1
    #     if n_l % 2 == 0:
    #         n_s = n_l / 2
    #         n_add = 0
    #     else:
    #         n_s = (n_l - 1) / 2
    #         n_add = 1
    # else:
    #     n_l = number_lines
    #     if n_l % 2 == 0:
    #         n_s = n_l / 2
    #         n_add = 0
    #     else:
    #         n_s = (n_l - 1) / 2
    #         n_add = 1
    # for n in range(int(number_stiches)):
    #     fd(length)
    #     penup()
    #     fd(length)
    #     pendown()
    # fd(n_add * length)
    # move back
    penup()
    rt(180)
    fd(((2*number_stitches)+1) * length)  # verändert
    # look again "forward"
    rt(180)
    pendown()


satz1 = "Heute habe ich endlich meine Python-Aufgaben geschafft."  # "Morgen wird die Sonne scheinen"
satz2 = "Ich bin stolz auf mich, das habe ich gut gemacht."  # "Heute aber regnet es, schade, oder?"

list1 = gen_binary_list(satz1)
list2 = gen_binary_list(satz2)

for element in list1:
    speed(0)
    stitch(element, len(list1), len(list2), stichlaenge)  # hier verändert
    rt(90)
    penup()
    fd(stichlaenge)
    lt(90)
    pendown()

# move to correct position for vertical:
penup()
lt(90)
fd(stichlaenge * (len(list1)))  # +1 rausgenommen
rt(180)
pendown()

for element in list2:
    stitch(element, len(list2), len(list1), stichlaenge)  # hier verändert

    lt(90)
    penup()
    fd(stichlaenge)
    rt(90)
    pendown()

exitonclick()