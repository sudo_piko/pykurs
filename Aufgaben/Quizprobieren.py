


quiz_daten = {"3+5": "8", "Tier?": "nein"}

def kennt_die_richtige_antwort(frage, richtige_antwort):
    gegebene_antwort = input(frage + " ")
    if gegebene_antwort == richtige_antwort:
        print("Richtig!")
        return True
    else:
        print("Falsch!")
        return False


name = input("Hallo! Willkommen zum Quiz. Wer bist Du? ")


punkte = 0

for f in quiz_daten:
    if kennt_die_richtige_antwort(f, quiz_daten[f]):
        punkte += 1
    print(f"derzeitiger Punktestand: {punkte}; letzte Frage: {f}")

anzahl_fragen = len(quiz_daten)

print(f"Quiz beendet! Erreichte Punkte: {punkte} von {anzahl_fragen} möglichen Punkten.")