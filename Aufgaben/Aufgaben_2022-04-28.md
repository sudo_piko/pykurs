# Aufgaben am 

# 0 - Lesestoff

## Stackoverflow: Reihenfolge der Import-Statements

## 1 – Hatchery
Schaut Euch die Hatchery genauer an: https://badge.team/
- Wie ist die Ratio zwischen allen Apps und den Apps für die Card10?
- Was ist die am besten bewertete App für die Card10?


## 2 - Apps

Sucht Euch eine App in der Hatchery aus und schaut Euch den Quellcode an. Zum Beispiel:

https://badge.team/projects/north_sense

### 2.1 Installieren
Installiert diese App und probiert sie aus!
Eine Installationsanleitung findet Ihr hier:  
https://card10.badge.events.ccc.de/userguide/general-usage/#installing-apps-via-usb
(Es ist aber eigentlich nur: Datei downloaden, entpacken, auf die Card10 kopieren.)

### 2.2 Verändern
Schließt die Card10 wieder an den Computer an und schaut Euch nochmal den Quellcode auf der Card10 an. 
Verändert einen Teil des Codes und probiert aus, ob es funktioniert hat!


## 3 – Mehr Range
(Wenn Ihr letztes Mal die Aufgabe 3 nicht gemacht habt, dann macht die Aufgabe von letztem Mal statt dieser hier.)
Letztes Mal solltet Ihr eine Funktion bauen, die range(x) ersetzen kann. Verändert sie, sodass sie range(x, y) ersetzen kann!

```python
for i in range(5, 20):
    print("Zweimal", i, "ist:", i*2)
```
In diesem Code-Beispiel sollt Ihr "range(5, 20)" durch "euer_funktionenname(5, 20)" ersetzen können. (Natürlich soll in der Funktionsdefinition kein range() vorkommen... ;-)