# Aufgaben am 

# 0 - Lesestoff
https://www.pythonforbeginners.com/files/reading-and-writing-files-in-python

## 1 – Dateien öffnen
https://www.w3schools.com/python/python_file_open.asp

- Erstellt eine Liste mit 20 Personennamen (personen.txt) und eine zweite Liste mit 20 Zahlen (alter.txt)
- Baut ein Python-Programm, das die Personenliste ausgibt. Orientiert Euch dafür an den Dateien in Kleines/Beispielordner_open_files
- Baut ein Python-Programm, das überprüft, ob in der Liste der Personennamen ein x vorkommt. Und ein j! und wie viele e?
- schaut Euch die string-Methode "split" an! Sucht im Internet nach ihrer Funktionsweise.
<details> 
  <summary>Tipps zu .split()</summary>
Was tut folgendes Programm?
   ```python
satz = "euai aeu eaui eaiu uea eaui eua"
l = satz.split()
print(l)
```
</details>
- Baut ein Python-Programm, das die Personenliste so ausgibt, dass jeder Name zweimal hintereinander kommt:
```
KimKim
AlexisAlexis
...
```
- Baut ein Python-Programm, das die Personen mit den Altern ausgibt:
```
Kim ist 18 Jahre alt.
Alexis ist 27 Jahre alt.
...
```

## Card10 verstehen
Verändert den Teatimer so, dass er, wenn der Tee fertig ist, eine Prideflag mit den LEDs anzeigt.

Das ist pride.show_leds(); vorher müsst Ihr natürlich auch noch pride importieren.
https://firmware.card10.badge.events.ccc.de/pycardium/pride.html

Um den Teatimer besser zu verstehen, findet Ihr im Ordner "Lösungen zu Aufgaben" eine kommentierte Version von Desiderat.

## 3 – Card10-Projekte
Sammelt Ideen!  
https://md.ha.si/KyNpRRqCRnK3866EroL9bg#