# Einfaches Pong-Spiel
# GPL
# nach einer Vorlage aus dem Internet™ von @TokyoEdTech: https://github.com/wynand1004/Projects/tree/master/Pong
# Piko, 4.1.2022

import turtle
import time

fenster = turtle.Screen()
fenster.title("Pikopong")
fenster.bgcolor("black")
fenster.setup(width=800, height=600)
fenster.tracer(0)

name_a = turtle.textinput("Pikopong", "Name der ersten Spieler*in")
name_b = turtle.textinput("Pikopong", "Name der zweiten Spieler*in")

punkte_a = 0
punkte_b = 0

# Spieler*innen
schlaeger_a = turtle.Turtle()
schlaeger_b = turtle.Turtle()

for schlaeger in schlaeger_a, schlaeger_b:
    schlaeger.speed(0)
    schlaeger.shape("square")
    schlaeger.color("white")
    schlaeger.turtlesize(stretch_len=1, stretch_wid=5)
    schlaeger.penup()

schlaeger_a.goto(-350, 0)
schlaeger_b.goto(350, 0)

ball = turtle.Turtle()
ball.speed(0)
ball.shape("square")
ball.color("white")
ball.penup()
ball.goto(0, 0)
ball.xbewegung = 2
ball.ybewegung = 2

# Punktestandausgabe
stift = turtle.Turtle()
stift.speed(0)
stift.shape("square")
stift.color("white")
stift.penup()
stift.hideturtle()
stift.goto(0, 260)

def punktestand():
    ausgabetext = name_a + ": " + str(punkte_a) + "   " + name_b + ":" + str(punkte_b)
    stift.write(ausgabetext, align="center", font=("Courier", 20, "normal"))

punktestand()


# Funktionen für die Bewegung
def schlaeger_a_hoch():
    y = schlaeger_a.ycor()
    y += 20
    schlaeger_a.sety(y)

def schlaeger_b_hoch():
    y = schlaeger_b.ycor()
    y += 20
    schlaeger_b.sety(y)

def schlaeger_a_runter():
    y = schlaeger_a.ycor()
    y -= 20
    schlaeger_a.sety(y)

def schlaeger_b_runter():
    y = schlaeger_b.ycor()
    y -= 20
    schlaeger_b.sety(y)



# Tasten
fenster.listen()
fenster.onkeypress(schlaeger_a_hoch, "w")
fenster.onkeypress(schlaeger_a_runter, "s")
fenster.onkeypress(schlaeger_b_hoch, "Up")
fenster.onkeypress(schlaeger_b_runter, "Down")

# das eigentliche Spiel
while True:
    fenster.update()

    # Ballbewegung:
    # Geradeaus
    time.sleep(0.001)
    ball.setx(ball.xcor() + ball.xbewegung)
    ball.sety(ball.ycor() + ball.ybewegung)

    # Abprallen
    if ball.ycor() > 290:
        ball.sety(290)
        ball.ybewegung *= -1

    elif ball.ycor() < -290:
        ball.sety(-290)
        ball.ybewegung *= -1

    # Ball fliegt aus dem Spielfeld
    if ball.xcor() > 350:
        punkte_a += 1
        stift.clear()
        punktestand()

        ball.goto(0, 0)
        ball.xbewegung *= -1

    if ball.xcor() < -350:
        punkte_b += 1
        stift.clear()
        punktestand()

        ball.goto(0, 0)
        ball.xbewegung *= -1

    # Ball zurückschlagen
    if ball.xcor() < -340 and ball.ycor() < schlaeger_a.ycor() + 50 and ball.ycor() > schlaeger_a.ycor() - 50:
        ball.xbewegung *= -1

    if ball.xcor() > 340 and ball.ycor() < schlaeger_b.ycor() + 50 and ball.ycor() > schlaeger_b.ycor() - 50:
        ball.xbewegung *= -1


