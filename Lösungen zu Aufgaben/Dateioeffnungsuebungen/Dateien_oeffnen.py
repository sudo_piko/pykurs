
pfad = "personen.txt"

f = open(pfad)
inhalt = f.read()

g = open("alter.txt")
alterstring = g.read()


# if "x" in inhalt:
#     print("Es ist ein x vorhanden!")
#
# if "j" in inhalt:
#     print("Es ist ein j vorhanden.")
#
# i = 0
# for buchstabe in inhalt:
#     if buchstabe == "e":
#         i += 1
# print(i)
#
# n = inhalt.count("x")
# print(n)
#
# personen = inhalt
#
#
# for person in personen:
#     if "x" in personen:
#         anzahl_x = personen.count("x")
#     else:
#         print("Es gibt kein x.")        # funktioniert
#     if "j" in personen:
#         anzahl_j = personen.count("j")
#     if "e" in personen:
#         anzahl_e = personen.count("e")
# print("Es gibt", anzahl_j,"j.")
# print("Es gibt", anzahl_e,"e.")
#


namenliste = inhalt.split()
alterliste = alterstring.split()

# beidesliste = [("phoibi", "schokoladeneis"), ("desiderat", "Salted Caramel"), ("tinoca", "Mangosorbet")]
#
# name, sorte = "piko", "Brombeersorbet"
# name, sorte = sorte, name
#
# for xxxxxxxxxxxxxxxx in eine_liste:
# for person, eissorte in :
#     print(person, "bekommt", eissorte)

# zipliste = zip(namenliste, alterliste)
# print(list(zipliste))

for name, alter in zip(namenliste, alterliste):
    print(f"{name} ist {alter} Jahre alt.")
    # print(name+name)
