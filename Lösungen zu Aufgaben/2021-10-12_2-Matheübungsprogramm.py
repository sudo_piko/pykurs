# 19.10.2021
# Lösung der zweiten Aufgabe
# Piko

# ## VERSION 1
# import random
#
# ANZAHL_VERSUCHE = 3
# ANZAHL_AUFGABEN = 2
#
# punkte = 0
#
# for j in range(ANZAHL_AUFGABEN):
#     rand1 = random.randint(1, 20)
#     rand2 = random.randint(1, 20)
#     richtige_antwort = rand1 + rand2
#     inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2) + " "
#     for i in range(ANZAHL_VERSUCHE):
#         answer = input(inputfrage)
#         answer = int(answer)
#         if answer == richtige_antwort:
#             print("    Richtig")
#             punkte = punkte + 1
#             break
#         else:
#             if i == ANZAHL_VERSUCHE - 1:
#                 print("    Falsch! Das war dein letzter Versuch!")
#                 break
#
#             if abs(answer - richtige_antwort) == 1:
#                 # andere Versionen möglich! zB: if answer + 1 == richtige_antwort or answer - 1 == richtige_antwort
#                 print("    Leider falsch! Aber du lagst nur um eins daneben. Versuch es nocheinmal! Verbleibende Versuche:", (ANZAHL_VERSUCHE - (i + 1)))
#             else:
#                 print("    Falsch! Versuche es nocheinmal! Verbleibende Versuche:", (ANZAHL_VERSUCHE - (i + 1)))
#     print("            Punktestand:", punkte)
#
# print("            Punktestand: " + punkte)





# VERSION 2 – diese Version habe ich in der Stunde entwickelt
import random

ANZAHL_VERSUCHE = 3
ANZAHL_AUFGABEN = 4
SCHWIERIGKEITSGRAD = 10

punkte = 0


for j in range(ANZAHL_AUFGABEN):
    rand1 = random.randint(1, SCHWIERIGKEITSGRAD)
    rand2 = random.randint(1, SCHWIERIGKEITSGRAD)
    inputfrage = "was ist: " + str(rand1) + " plus " + str(rand2)


    for i in range(ANZAHL_VERSUCHE):
        answer = input(inputfrage)
        answer = int(answer)
        if answer == rand1 + rand2:
            print("Richtig")
            punkte = punkte + 1
            break
        else:
            if i == ANZAHL_VERSUCHE - 1:
                print("Falsch! Das war dein letzter Versuch!")
                break
            if abs(answer - (rand1+rand2)) == 1:
                print("Du liegst nur um 1 daneben!")
            print("Falsch! Versuche es nocheinmal! Verbleibende Versuche:", (ANZAHL_VERSUCHE - (i + 1)))


    print("Aktueller Punktestand: " + str(punkte))