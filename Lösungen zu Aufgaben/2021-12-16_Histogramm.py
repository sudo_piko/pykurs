gedicht = """The Python
by Joseph Hilaire Pierre Belloc
A Python I should not advise, —
It needs a doctor for its eyes,
And has the measles yearly.
However, if you feel inclined
To get one (to improve your mind,
And not from fashion merely),
Allow no music near its cage;
And when it flies into a rage
Chastise it, most severely.
I had an Aunt in Yucatan
Who bought a Python from a man
And kept it for a pet.
She died, because she never knew
These simple little rules and few; —
The snake is living yet."""

alphabet = "abcdefghijklmnopqrstuvw"

h = {}

for buchstabe in gedicht.lower():
    h[buchstabe] = h.get(buchstabe, 0) + 1

print(h)

import numpy as np
import matplotlib.pyplot as plt

pos = np.arange(len(h.keys()))
width = 10.0     # gives histogram aspect to the bar diagram

ax = plt.axes()
ax.set_xticks(pos + (width / 2))
ax.set_xticklabels(h.keys())

plt.bar(h.keys(), h.values(), width, color='g')
#
plt.show()

