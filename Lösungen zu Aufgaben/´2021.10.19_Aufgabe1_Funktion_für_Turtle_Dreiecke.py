#1 – Turtle
#Definiere jeweils eine Funktion, die

#eine Zahl übernimmt und ein Dreieck mit entsprechender Kantenlänge malt! Die würde eins dann so aufrufen: dreieck(100)
#zwei Zahlen übernimmt und ein Dreieck malt, mit entsprechender Kantenlänge und entsprechender Strichdicke => dreieck_mit_dicke(200, 20)
#eine (mit der Turtle sehr nützliche Funktion), die die Turtle von einer Stelle zur nächsten springen lässt, ohne dass sie malt. Wenn ich also zwei Dreiecke nebeneinander malen will, könnte das dann so aussehen:

#dreieck(130)
#jump(200)
#dreieck(50)

from turtle import *

def dreieck(laenge):  #wir definieren die Funktion Dreieck, mit der Variable laenge
    for i in range(3):
        fd(laenge) #die turtle soll so weit vorwärts gehen, wie in der Variable angegeben
        rt(120)


def dreieck_mit_dicke(laenge,dicke):  # wir definieren die Funktion Dreieck, mit der Variable laenge
    pensize(dicke) #Vorsicht, die pensize wird hier nicht zurückgesetzt, sowas kann unangenehm werden
    for i in range(3):
        fd(laenge)  # die turtle soll so weit vorwärts gehen, wie in der Variable angegeben
        rt(120)


def jump(laenge):
    pu()
    fd(laenge)
    pd()

#und der Test:

dreieck(130)
jump(200)
dreieck(50)

#Variante: nach der laenge als input fragen :)
#numinput macht sogar ein popup in der turtle auf
