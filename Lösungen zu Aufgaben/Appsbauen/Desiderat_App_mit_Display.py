# Neue App schreiben aus Pikos Kurs
# Was passiert hier?: Die erste LED an geht, dann die 2te LED, dann rechts grüne LED hochdimmen
# Dann Aufforderung Button lins zu drücken und es geht blaue LED an wenn gedrückt.
# Das kann man wiederholen bis man rechten button drückt.

CONFIG_NAME = 'NeueTestApp.json'
import leds
import utime
import display
import buttons
import color

#### LED gehen an und aus: ###############################
leds.set_rocket(0, 31)  # Farbe und Helligkeit von 0 -31, also 2 hoch 5
utime.sleep(0.7)
leds.set_rocket(1, 31)
utime.sleep(0.7)
leds.set_rocket(0, 0)  # LED nr. 0 wird ausgeschaltet
leds.set_rocket(1, 0)  # LED nr. 1 wird ausgeschaltet
utime.sleep(0.2)

############## Hochdimmen: ###################################
led = 2  # grün rechts
for i in range(32):
    leds.set_rocket(led, i)
    utime.sleep(0.2)  # Warte 200ms
leds.set_rocket(led, 0)
utime.sleep(0.5)
# letzte LED ausschalten fehlt noch


###########Etwas auf dem Display anzeigen und Buttons verwenden:##########
# Das passiert: Blaue LED anschalten, wenn ich Button klicke
# soll sie länger leuchten, dann muss ich sleep verwenden

# Anzeige auf Display:
with display.open() as d:
    d.clear()
    d.print('Press bottom left', fg=[122, 86, 183], bg=None, posx=0, posy=5,
            font=display.FONT16)  # Es gibt nicht alle Schriftgrößen
    d.print('for LED!', fg=[122, 86, 183], bg=None, posx=0, posy=15, font=display.FONT16)
    d.print('Press bottom right', fg=[192, 91, 176], bg=None, posx=0, posy=40, font=display.FONT16)
    d.print('for exit!', fg=[192, 91, 176], bg=None, posx=0, posy=55, font=display.FONT16)
    d.update()  ## Das Objekt d soll nun das oben anzeigen (=Auslösen)

###########
while True:
    pressed = buttons.read(
        buttons.BOTTOM_LEFT | buttons.BOTTOM_RIGHT
    )
    # if pressed != 0:
    #   break
    if pressed & buttons.BOTTOM_LEFT != 0:  # linker Button bewirkt Blinken
        # print("Left button pressed!")
        # with display.open() as d:
        #   d.clear()
        #   d.print('Press bottom left für LED:', font=display.FONT12) # Größere Schrift geht nicht, da zu lang
        #  d.update()

        leds.set_rocket(0, 31)  # LED Nr. 0 geht an mit Helligkeit 31
        utime.sleep(2)  # Leuchtzeit der LED
        leds.set_rocket(0, 0)  # Schaltet LED Nr.0 aus
    if pressed & buttons.BOTTOM_RIGHT != 0:  # rechter Button bewirkt Beenden
        # print("Right button pressed!")
        break  # Endlosschleife unterbrechen


