# Die erste und allereinfachste Version

# Zweite Version mit Buttons

import leds
import utime
import buttons

leds.set_rocket(0, 31)
utime.sleep(0.5)
leds.set_rocket(1, 31)
utime.sleep(0.5)
leds.set_rocket(0, 0)
leds.set_rocket(1, 0)
utime.sleep(1)

led = 2
for i in range(32):
    leds.set_rocket(led, i)
    utime.sleep(0.1)

utime.sleep(1)

print("Press bottom left or right button:")  # Das print geht noch nicht...

while True:  # Wir durchlaufen die Schleife für immer...
    pressed = buttons.read(
        buttons.BOTTOM_LEFT | buttons.BOTTOM_RIGHT
    )

    if pressed & buttons.BOTTOM_LEFT != 0:  # Wenn der linke Button gepresst wird,
        print("Left button pressed!")
        leds.set_rocket(0, 31)              # machen wir die LED Nr. 0 an,
        utime.sleep(4)                      # warten 4 Sekunden
        leds.set_rocket(0, 0)               # und machen sie wieder aus.

    if pressed & buttons.BOTTOM_RIGHT != 0:
        print("Right button pressed!")
        break  # Dieses break-statement unterbricht die ewige Schleife

# Das hier ist das Ende des Programms. Wenn die Card10 bis hierhin kommt,
# macht sie dann wieder das Menü an.