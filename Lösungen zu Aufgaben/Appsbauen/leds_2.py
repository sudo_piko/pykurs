# Die erste und allereinfachste Version


import leds
import utime

leds.set_rocket(0, 31)
utime.sleep(0.5)
leds.set_rocket(1, 31)
utime.sleep(0.5)
leds.set_rocket(0, 0)
leds.set_rocket(1, 0)
utime.sleep(1)

led = 2
for i in range(32):
    leds.set_rocket(led, i)  # Langsam die dritte LED hochfaden
    utime.sleep(0.3)

leds.set_rocket(led, 0)  # die dritte LED wieder ganz ausmachen