# 19.10.2021
# Ausführliche Lösung der ersten Aufgabe
# Piko


import math
from turtle import *

## Wie in der Turtle-Dokumentation ein Stern gemacht wird:
# color('red', 'yellow')
# begin_fill()
# while True:
#     forward(200)
#     left(170)
#     if abs(pos()) < 1:
#         break
# end_fill()
# done()

def stern(laenge, zacken):
    # zwei sterne übereinanderlegen, wenn es 6 oder 10 zacken sind:
    if zacken == 6:
        stern(laenge, int(zacken/2))
        rt(30)
        pu()
        distanz = 2 * math.sqrt((laenge/3)**2 - (laenge/6)**2)  # Blut, Schweiß, Pythagoras und Tränen
        fd(distanz)
        pd()
        lt(90)
        stern(laenge, int(zacken/2))
    elif zacken == 10:
        stern(laenge, int(zacken/2))
        pu()
        radius = 0.526 * laenge  # aus: https://de.wikipedia.org/wiki/Pentagramm
        lt(18)  # die Zacken haben einen Winkel von 36°, das wollen wir halbieren.
        # jetzt guckt die Turtle genau in die Mitte.
        fd(radius)  # jetzt ist sie in der Mitte
        fd(radius)  # jetzt ist sie genau auf der anderen Seite
        lt(180-18)  # umdrehen und 18° zurück, damit wir mit den nächsten Zacken anfangen können.
        pd()
        stern(laenge, int(zacken/2))
    elif zacken < 3:
        print("nope. Kein Stern.")
    elif zacken == 4:
        # hier schönen Stern einfügen
        for i in range(4):
            fd(laenge)
            rt(90)
        print("Du wolltest es nicht anders.")
    elif zacken % 2 == 0:
        if int(zacken/2) in [7,9,11]:
            # Spezialfall mit Doppelstern? Hier müsste noch etwas gebaut werden.
            pass
        # hier müsste eins noch einen schönen Stern machen
        zackenwinkel = 180 - (360/zacken)
        for i in range(zacken):
            fd(laenge)
            lt(zackenwinkel)

    else:
        # Ungerade Zackenanzahl ist das beste.
        zackenwinkel = (360/zacken) * (zacken//2)
        for i in range(zacken):
            fd(laenge)
            lt(zackenwinkel)

speed(10)


# EINFACHE LÖSUNG
for zackenzahl in range(3, 20):
    if zackenzahl % 2 == 1:
        winkel = (360/zackenzahl) * (zackenzahl//2)
        for j in range(zackenzahl):
            fd(100)
            lt(winkel)
    clear()

done()




from turtle import *
ANZAHL_STERNE = 4

zackenzahl = 7
ZACKENZAHL = 8

speed(100)
color("red", "yellow")


begin_fill()
for i in range(ANZAHL_STERNE):
    if zackenzahl % 2 == 1:
        winkel = (360/zackenzahl) * (zackenzahl//2)
        for j in range(zackenzahl):
            fd(100)
            lt(winkel)
    pu()
    fd(200)
    pd()
end_fill()

done()