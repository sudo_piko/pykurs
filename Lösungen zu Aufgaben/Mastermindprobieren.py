# 13.01.2022
# Mastermind – Skizze

import turtle, random

# Turtle-Konstanten
KREISRADIUS = 30
KONTUR = 2
DISTANZ = 2 * KREISRADIUS + 10

# Spiel-Konstanten
N_SLOTS = 4

# sechs Farben sagt Wikipedia
farben = ["red", "orange", "yellow", "green", "blue", "purple"]
zeile = random.choices(farben, k=N_SLOTS)

print(zeile)  # zum Debuggen, wenn eins zu faul ist, wirklich nachzudenken.

# Turtle setup
fenster = turtle.Screen()
fenster.setup(width=1400, height=800)  # Das hier noch an die Slots anpassen
turtle.speed(0)
turtle.hideturtle()
turtle.pu()
turtle.goto(-100, 280)


# Funktionen für das Spiel
def farbkreis(farbe):
    turtle.pd()
    turtle.pensize(KONTUR)
    turtle.fillcolor(farbe)
    turtle.begin_fill()
    turtle.circle(KREISRADIUS)
    turtle.end_fill()
    turtle.pu()

def zeile_malen(zeile):
    for farbe in zeile:
        farbkreis(farbe)
        turtle.fd(DISTANZ)
    turtle.fd(-1 * DISTANZ * len(zeile))
    turtle.right(90)
    turtle.fd(DISTANZ)
    turtle.lt(90)

def versuch_alt():
    versuch = input("Rate Farben! ").split()
    # später vielleicht mit Klicken? Oder andere Lösung?
    zeile_malen(versuch)

    hinweis = []
    betrachtete_stellen = []
    zeile_helper = zeile.copy()  # Hier in die Falle laufen lassen
    # versuch_helper = versuch.copy()
    for i, f in enumerate(versuch):
        if f in zeile and zeile[i] == f:
            hinweis.append("black")
            betrachtete_stellen.append(i)  # ist jetzt [1]
            zeile_helper[i] = "x"
            versuch[i] = "x"
            # print(zeile_helper)
            if zeile_helper == ["x" for _ in zeile_helper]:   # ["x", "x", "x", "x"]
                print("Gewonnen!")
                return True
    for i, f in enumerate(versuch):
        if f != "x":
            if f in zeile_helper:
                hinweis.append("grey")
                zeile_helper[zeile_helper.index(f)] = "x"  # eleganter wäre: zeile_helper.replace(f, "x") ???
            else:
                hinweis.append("white")
    hinweis.sort()
    zeile_malen(hinweis)

def versuch():  # neue Version: eleganter
    versuch = input("Rate Farben! ").split()
    # später vielleicht mit Klicken? Oder andere Lösung?
    zeile_malen(versuch)
    schwarz = 0
    grau = 0
    betrachtete_stellen = []
    zeile_helper = zeile[:]
    for i, f in enumerate(versuch):  # Erster Durchgang: ist irgendwas genau richtig?
        if zeile[i] == f:
            schwarz += 1
            betrachtete_stellen.append(i)
            zeile_helper[i] = "x"
            versuch[i] = "x"
            # print(zeile_helper)
            if zeile_helper == ["x"] * len(zeile_helper):   # ["x", "x", "x", "x"]
                print("Gewonnen!")
                return True
    for i, f in enumerate(versuch):  # Zweiter Durchgang: ist es ungefähr richtig?
        if f != "x":
            if f in zeile_helper:
                grau += 1
                zeile_helper[zeile_helper.index(f)] = "x"
                # eleganter wäre: zeile_helper.replace(f, "x") – gibt es aber nicht für Listen!
                # alternativ könnte man das mit einer List-Comprehension machen.
    hinweis = ["black"] * schwarz + ["grey"] * grau + ["white"] * (N_SLOTS - (schwarz + grau))

    zeile_malen(hinweis)

fertig = False
while not fertig:
    fertig = versuch()
