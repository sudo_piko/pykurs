#3 – def Verknüpfungen

#Schreibe eine Funktion, die einen String übernimmt und ihn umdreht, also aus "rückwärts" "sträwkcür" macht. (Vorsicht beim Funktionsnamen: Was passiert, wenn da Umlaute drin sind?)
#Schreibe eine Funktion, die die rückwärts-Funktion verwendet. So soll sie aussehen:

#>>> drehen_und_oft_ausgeben("al", 3)
#lalala
#>>> drehen_und_oft_ausgeben("hui", 1):
#iuh

def rueckwaerts(wort):
    ausgabe = "" #Ausgabe ist erstmal leer, hier soll das umgedrehte Wort rein
    while len(wort) > 0: #so lange es das wort noch gibt
        ausgabe = ausgabe + wort[-1] #ausgabe nehmen, plus das letzte element von wort
        wort = wort[:-1] #wort bis zur vorletzten Stelle ist jetzt das was in wort ist
        print (wort, ausgabe) #das hier ist nur zur Kontrolle, ob die funktion tut was sie soll
    return ausgabe

def drehen_und_oft_ausgeben(wort,anzahl):
    trow =rueckwaerts(wort)
    print(trow*anzahl)

drehen_und_oft_ausgeben("haecksen",5)

