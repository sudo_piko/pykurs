# Kleines Stimmungsaufhellprogramm



import datetime  # Piko dachte ursprünglich, dass es "time" wäre, aber es ist das Modul "datetime"!


eingabe = input("Wie geht es Dir auf eine Skala von 0 bis 10? ")
# wochentag = input("Welcher Wochentag ist heute? ")  # statt wochentag später händisch zu definieren.

stimmungszahl = int(eingabe)  # in eingabe ist jetzt ein integer gespeichert
wochentag = "sunday"


# dritte Möglichkeit:
# heute = datetime.date.today()
# wochentag = heute.weekday()  # wochentag ist jetzt ein integer von 0 (Montag) bis 6 (Sonntag).
#  Das muss dann im if-Statement angepasst werden.

print(stimmungszahl == 5)


if stimmungszahl < 5 and wochentag == "sunday":
    print("Iss mehr Schokolade!")


# Teil 2

verbotene_person1 = "Piko"
verbotene_person2 = "melzai"
verbotene_person3 = "janepie"

name = input("Wer bist du? ")
if name == verbotene_person1:
    print("Du darfst hier nicht rein.")


