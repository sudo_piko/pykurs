# Pikos Python Kurs am 07.04.2022

## Pads
Ursprünglicher Code von Desiderat
https://md.darmstadt.ccc.de/ARJ3Wb92ThWi_w6CAZVhOQ?both#


## Pikos Datei

Code von Desiderat mit einigen Veränderungen von Piko
```python
import turtle


class Dreieck:
    def __init__(self, laenge, farbe, fuellfarbe, strichdicke, winkelrundung):
        self.laenge = laenge
        self.farbe = farbe
        self.fuellfarbe = fuellfarbe
        self.strichdicke = strichdicke
        self.winkelrundung = winkelrundung

    def malen(self, info_ausgeben=True, offset=0):
        t = turtle.Turtle()
        t.ht()
        t.pu()
        t.fd(offset)
        t.pd()
        t.pensize(2)
        t.color(self.farbe, self.fuellfarbe)
        t.write(f"Größe: {self.laenge}")
        t.begin_fill()
        for i in range(3):
            t.fd(self.laenge - 2 * self.winkelrundung)
            # t.lt(120)  # mit spitzen Ecken
            t.circle(self.winkelrundung, extent=120)
        t.end_fill()
        return t

    # Beide Farben ändern:
    def farben_aendern(self, stiftfarbe_aendern, fuellfarbe_aendern):
        print("jetzige Farben sind vorher: ", self.farbe, self.fuellfarbe)
        t = turtle.Turtle()
        t.ht()
        t.pu()
        t.fd(100)
        t.pd()
        t.pensize(2)
        self.farbe = stiftfarbe_aendern  # Ändert Mutterobjekt das_dreieck
        self.fuellfarbe = fuellfarbe_aendern
        t.color(stiftfarbe_aendern, fuellfarbe_aendern)  # Ändert Turtleobjekt
        t.begin_fill()
        for i in range(3):
            t.fd(self.laenge - 2 * self.winkelrundung)
            # t.lt(120)  # mit spitzen Ecken
            t.circle(self.winkelrundung, extent=120)
        t.end_fill()
        print("jetzige Farben sind nachher: ", self.farbe, self.fuellfarbe)

    # Farbe Stift änden:
    def stiftfarbe_aendern(self, farbe, offset):
        t = turtle.Turtle()
        t.pu()
        t.ht()
        t.fd(offset)
        t.pd()
        t.pensize(2)
        t.pencolor(farbe)
        t.fillcolor(self.fuellfarbe)
        t.fillcolor()
        t.begin_fill()
        for i in range(3):
            t.fd(self.laenge - 2 * self.winkelrundung)
            # t.lt(120)  # mit spitzen Ecken
            t.circle(self.winkelrundung, extent=120)
        t.end_fill()

    # Füllfarbe ändern:
    def fuellfarbe_aendern(self, fuellfarbe):
        t = turtle.Turtle()
        t.pu()
        t.ht()
        t.fd(200)
        t.pd()
        t.pensize(2)
        t.fillcolor(fuellfarbe)
        t.begin_fill()
        for i in range(3):
            t.fd(self.laenge - 2 * self.winkelrundung)
            # t.lt(120)  # mit spitzen Ecken
            t.circle(self.winkelrundung, extent=120)
        t.end_fill()


# Aufrufe:
offset = 0
turtlelein = turtle.Turtle()
das_dreieck = Dreieck(40, "red", "green", 3, 10)
das_dreieck.malen("Größe: ", True, offset)  # t.write wird ausgeführt
das_dreieck.farben_aendern("blue", "yellow", offset)
# das_dreieck.malen()

print(das_dreieck.farbe)

das_dreieck.stiftfarbe_aendern("orange", 150)
das_dreieck.fuellfarbe_aendern("lightgreen", offset)

turtle.exitonclick()
```

Andere Lösung: Nur das Objekt verändern, nicht das Dreieck nochmal malen 
```python
import turtle

class Dreieck:
    def __init__(self, laenge, farbe, fuellfarbe, strichdicke, winkelrundung):
        self.laenge = laenge
        self.farbe = farbe
        self.fuellfarbe = fuellfarbe
        self.strichdicke = strichdicke
        self.winkelrundung = winkelrundung

    def malen(self, info_ausgeben):
        t = turtle.Turtle()
        t.pensize(self.strichdicke)
        t.color(self.farbe, self.fuellfarbe)
        t.write(f"Größe: {self.laenge}")
        t.begin_fill()
        for i in range(3):
            t.fd(self.laenge - 2 * self.winkelrundung)
            # t.lt(120)  # mit spitzen Ecken
            t.circle(self.winkelrundung, extent=120)
            t.circle(steps=3, radius=100)
        t.end_fill()

    def farben_aendern(self, neue_farbe=None, neue_fuellfarbe=None):
        if neue_farbe != None:
            self.farbe = neue_farbe
        if neue_fuellfarbe != None:
            self.fuellfarbe = neue_fuellfarbe
            # lexis_dreieck.fuellfarbe = neue_fuellfarbe

lexis_dreieck = Dreieck(50, "blueviolet", "purple", 5, 7)
lexis_dreieck.malen(True)
lexis_dreieck.farben_aendern("green", "blue")
lexis_dreieck.farben_aendern(neue_fuellfarbe="blue", neue_farbe="green")
lexis_dreieck.malen(True)
lexis_dreieck.farben_aendern("black")
lexis_dreieck.malen(True)
lexis_dreieck.farben_aendern(neue_fuellfarbe="white")



turtle.exitonclick()
```