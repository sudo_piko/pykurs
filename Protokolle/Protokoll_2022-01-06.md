# Pikos Python Kurs am 18.11.2021

## Mitschrift 
### Wiederholung
Was sind dictionaries? Antworten erst selbst notieren, dann mit Musterlösungen darunter abgleichen

1. What does the code for an empty dictionary look like?
leeres_dictionary = {}

2. What does a dictionary value with a key 'foo' and a value 42 look like?
d = {"foo":42} Struktur: erst kommt der key, dann der value

3. What is the main difference between a dictionary and a list?
list. Indexe sind Zahlen
dict: Indizes kann alles sein
-> die Art, wie man auf Werte zugreift ist anders

4. What happens if you try to access spam['foo'] if spam is {'bar': 100}?
Key-Error weil der key "foo" nicht im dict vorhanden ist


### Dict Übung mit Laptops
hatten wir so ähnlich schon mal mit den fahrrädern -> den Code kann man größtenteils übernehmen

wie macht man aus zwei verschiedenen dict ein Kombi-dict, in dem die jeweiligen Werte zu den keys aufsummiert sind? 

erst mal braucht man eine liste aller keys. 
liste_aller_keys = []

dann jeden key in den dictionaries einmal in die Hand nehmen und der Liste hinzufügen
```python
for k in d1.keys()
liste_aller_keys.append(k)
print(liste_aller_keys) # zum Überprüfen

```

jetzt werden noch die fehlenden keys hinzugefügt, die in d2 drin sind, aber nicht in d1 (und der bisherigen liste_aller_keys)
```python
for k in d2.keys()
if k not in liste_aller_keys:
liste_aller_keys.append(k)

```

nächster Schritt: die value-Summen bilden und in ein neues dict einfügen

```
neues_dict = {}

for k in liste_aller_keys:
wert1 = d1.get(k,0) # Default-wert soll 0 sein, sonst wird None ausgegeben und das lässt sich nicht addieren mit den anderen values (int)
wert2 = d2.get(k,0)
neues_dictionary[k] = wert1 + wert2 # dadurch wird die Summe jeweils in den key des neuen dict für den key "k" eingefüllt

```

Hinweis: Wertabfrage über 
wert1 = d1[k]
wert2 = d2[k] 
funktioniert in dem Fall nicht. Grund: der key "k" ist gar nicht in beiden listen enthalten. Deshalb brauchen wir die Methode dict.get()

Funfact: Bei dict ist die Reihenfolge der keys egal, bei listen nicht. Eine Liste mit den gleichen Werten aber unterschiedliche Reihenfolge ist nicht die gleiche. Ein dict wird von Python als gleich anerkannt (dict1 == dict2 ? -> ergibt True)

Wiederholung (Exkurs): zum Ausprobieren, um zu gucken, was man alles aus einem dictionary ausgeben kann
print(d1.keys(), d1.values(), d1.items())
bei d1.items() kommt eine Serie von tuples heraus. Die eckigen Klammern zeigen, dass es etwas listenartiges ist. 

Tipp
"if not in" kann auch bei der Histogramm aufgabe angewendet werden

Lösung von Julika zum Kombinieren von dictionaries
```

d_ges = {}
for element in d1:
    if element in d2:
        d_ges[element] = d1[element] + d2[element]
    else:
        d_ges[element] = d1[element]
for element in d2:
    if element not in d1:
        d_ges[element] = d2[element]        
print(d_ges)

```

# oder kürzer:
    
```

d_ges = {}
for element in d1:
    d_ges[element] = d1[element] + d2.get(element, 0)
for element in d2:
    if element not in d1:
        d_ges[element] = d2[element]
print(d_ges)

```

Gibt es recap-Wünsche oder Fragen, was wir noch mal durchnehmen sollten? Schreibt Piko eine Mail 

Beispiel Lösung für schöne Muster: [siehe unten]
nächster Schritt wäre, das ganze in eine Funktion zu gießen und diese vertikal versetzt auszuführen


## Pikos Datei


Hausaufgabe Aufgabe 1
```python
d1 = {'a': 100, 'b': 200, 'c':300}
d2 = {'a': 300, 'b': 200, 'd':400}
# Sample output: {'a': 400, 'b': 400, 'd': 400, 'c': 300})

liste_aller_keys = []
for k in d1.keys():
    liste_aller_keys.append(k)

for k in d2.keys():
    if k not in liste_aller_keys:
        liste_aller_keys.append(k)


#
# print(liste_aller_keys)
#
# if "x" not in liste_aller_keys:
#     print("huiuiuiuiui")


neues_dictionary = {}

for k in liste_aller_keys:
    wert1 = d1.get(k, 0)
    wert2 = d2.get(k, 0)
    neues_dictionary[k] = wert1 + wert2

musterloesungsdictionary = {'a': 400, 'b': 400, 'd': 400, 'c': 300}

print(neues_dictionary)
print(musterloesungsdictionary)
print(neues_dictionary == musterloesungsdictionary)
liste1 = ["a", "b", "c", "d"]
liste2 = ["a", "b", "d", "c"]
print(liste1 == liste2)
neues_dictionary["b"]



```


Anstoß für Histogramm-Aufgabe:
```python



gedicht = """The Python
by Joseph Hilaire Pierre Belloc
A Python I should not advise, —
It needs a doctor for its eyes,
And has the measles yearly.
However, if you feel inclined
To get one (to improve your mind,
And not from fashion merely),
Allow no music near its cage;
And when it flies into a rage
Chastise it, most severely.
I had an Aunt in Yucatan
Who bought a Python from a man
And kept it for a pet.
She died, because she never knew
These simple little rules and few; —
The snake is living yet."""

# print(gedicht.lower())

buchstabendictionary = {"e":0}

for buchstabe in gedicht.lower():
    if buchstabe == "e":
        buchstabendictionary[buchstabe] += 1

print(buchstabendictionary["e"])


```

Anstoß für Hitomezashi-Aufgabe
```python
import turtle


def strichlinie(anzahl):
    for i in range(anzahl-1):
        turtle.fd(20)
        turtle.pu()
        turtle.fd(20)
        turtle.pd()
    turtle.fd(20)

vokale = "aeiou"

text = "Ayliean MacDonald discusses Hitomezashi Stitch Patterns.More links & stuff in full descri"

for c in text.lower():
    if c in vokale:
        turtle.pu()
        turtle.fd(20)
        turtle.pd()
    strichlinie(10)
    if c not in vokale:
        turtle.pu()
        turtle.fd(20)
        turtle.pd()
    turtle.pu()
    turtle.backward(400)
    turtle.rt(90)
    turtle.fd(20)
    turtle.lt(90)
    
    
```

