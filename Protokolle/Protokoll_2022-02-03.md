# Pikos Python Kurs am 27.01.2022

## Mitschrift 

## Konstanten
konstanten= einmalig definierte (oft integers) sich nicht verändernde werte 
Ab Zeile 27 hier Beispiele für KONSTANTEN
https://github.com/anthonymonori/python-minigames/blob/master/pong.py

### äpfel birnen und so
es gibt 
* imperative programmierstile: Zeile 90-115 mach das mach das mach das
* objektorientierter programmierstil ..tbd
* funktionale programmierstile: wenn ich das machen würde, müsstest du das machen

funktionaler programmierstil ist, womit wir uns beschäftigen, also mit einkapseln und so. 

### Genauerer Blick auf den Code
print() ist immer gut zum testen, oder nochmal zwischenwerte auszuwerfen, wenn man nicht mehr genau weiss, was los is, teilweise auch mit "printstatement"

random.choices(farben, k=N_SLOTS)
arbeitet mit replacements
random.samples ohne replacements (also dubletten)

.split ist eine Methode um strings zu zerteilen

id() verweist auf speicherstellen, 
warum ändern sich listen, wenn man ihren inhalt ändert?
weil listen sich auf id() beziehen und wir mit der änderung die id() ändern
listen greifen auf diese id zu, und man verändert den inhalt, aber sie behalten die id

möchte ich ein listen original behalten, ist eine lösung
a ist liste
d ist [:] bedeutet erster bis letzer index bleiben unangetastet, während man in a dinge ändern kann, bleibt d das original a


strings können den inhalt nicht verändern, "str" object does not support item assignment


# Pads
Cordjackenpad mit den Aufgaben zu letztem Mal; Kommentare zu Konstanten und Kapseln und weiterem:  
https://md.ha.si/cMGZMFhDSNqmVxsTNvnBww#

Pad mit den Aufgaben zu diesem Mal:  
https://md.ha.si/VTjPab4PSQyUvat8TnLXBw#

## Pikos Datei
mit der Weiterentwicklung von einer der Aufgabenlösungen im Cordjackenpad.

```python

import turtle

# zeile_malen soll eine Zeile malen,
# egal, ob es jetzt beim setup ist, wo die Kreise grau sind (dann ist farbenliste eben ["silver", "silver"...],
# oder ein Versuch während des Spiels.


# Einfachste Funktion
# def zeile_malen(farbenliste):
#     for f in farbenliste:
#         kreis(f)

# # Hochkopiert
# def zeile_malen(farbenliste):
#     # for f in farbenliste:
#     #     kreis(f)
#     zeile(linienposition)
#     write(str(versuch), False, "center", ("arial", 13))
#     fd(50)
#     for i in range(4):
#         kreis("silver")
#     linienposition += 70
#     versuch -=1


# # Die Orte sind eigentlich nur von der Nummer des Versuchs abhängig
# def zeile_malen(farbenliste): # <- hier versuch rein.
#     for f in farbenliste:
#         kreis(f)
#     zeile(linienposition)  # linienposition müssen wir ausrechnen; ist von versuch abhängig.
#     write(str(versuch), False, "center", ("arial", 13))
#     fd(50)
#     for i in range(4):
#         kreis("silver")
#     linienposition += 70  # Linienposition wird pro versuch eins größer.
#     versuch -=1  # das kommt später raus, das kriegen wir ja als Parameter.

#
#
def zeile_malen(farbenliste, versuch): # versuch ist eine Zahl zwischen 1 und 12
    # for f in farbenliste:
    #     kreis(f)
    linienposition = -320 + (12-versuch) * 70
    zeile(linienposition)
    # weil es nicht funktioniert, aber keine Fehlermeldung ausgibt,
    # lasse ich mal linienposition (die Sache, mit der ich am unsichersten bin) printen.
    print("Linienposition:", linienposition)
    write(str(versuch), False, "center", ("arial", 13))
    fd(50)
    for f in farbenliste:
        kreis(f)

# Eigentlich wäre es aber noch schön, eine Funktion zu haben, die mir nur eine Kreisreihe malt, sodass ich sie auch für die Farbpalette verwenden kann.



farbpalette = ["darkmagenta", "goldenrod", "palevioletred", "firebrick", "steelblue", "olivedrab"]

turtle_einrichten("ivory") # Hintergrundfarbe
pu()
setx(-200)
sety(-390)
write("Deine Farben:", False, "left", ("arial", 15))
sety(-420)
fd(20)
for farbe in farbpalette:
    kreis(farbe)
setx(-200)
sety(-480)
write("lila - gelb - rosa - rot - blau - grün", False, "left", ("arial", 15, "italic"))

linienposition = -320
versuch = 12

for j in range(12):
    zeile(linienposition)
    write(str(versuch), False, "center", ("arial", 13))
    fd(50)
    for i in range(4):
        kreis("silver")
    linienposition += 70
    versuch -=1

#
for j in range(12, 0, -1):
    zeile_malen(["silver", "silver", "silver", "silver"], j)

exitonclick()
```

Die Lösung von Piko für Mastermind findet sich in der Datei Mastermindprobieren.py in *Lösungen zu Aufgaben*

Pikos Python-Shell zum Thema Listen-Kopieren und Identität von Objekten
```
>>> a = 2
>>> b = a
>>> c = 2
>>> for n in (a, b, c):
...     print(n)
... 
2
2
2
>>> id(a)
139720664531216
>>> id(b)
139720664531216
>>> id(c)
139720664531216
>>> b += 1
>>> b
3
>>> a
2
>>> for n in (a, b, c):
...     print(id(n))
... 
139720664531216
139720664531248
139720664531216
>>> a = "ui"
>>> b = a
>>> c = "ui"
>>> for n in (a, b, c):
...     print(n)
... 
ui
ui
ui
>>> b += "hui"
>>> for n in (a, b, c):
...     print(n)
... 
ui
uihui
ui
>>> for n in (a, b, c):
...     print(id(n))
... 
139720663378224
139720662559344
139720663378224
>>> a = [1, 2, 3]
>>> b = a
>>> c = [1, 2, 3]
>>> for l in (a,b,c):
...     print(l)
... 
[1, 2, 3]
[1, 2, 3]
[1, 2, 3]
>>> for l in (a,b,c):
...     print(id(l))
... 
139720663374080
139720663374080
139720663291136
>>> a is b
True
>>> a is c
False
>>> a
[1, 2, 3]
>>> c
[1, 2, 3]
>>> a += [4,5]
>>> a
[1, 2, 3, 4, 5]
>>> id(a)
139720663374080
>>> id(b)
139720663374080
>>> a
[1, 2, 3, 4, 5]
>>> b
[1, 2, 3, 4, 5]
>>> c
[1, 2, 3]
>>> a
[1, 2, 3, 4, 5]
>>> a[3] = "vier"
>>> a
[1, 2, 3, 'vier', 5]
>>> wort = "hallo"
>>> wort[3]
'l'
>>> wort[3] = "i"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: 'str' object does not support item assignment
>>> d = a[:]
>>> id(a)
139720663374080
>>> id(d)
139720663351744
>>> d = a.copy()
>>> d = a.deepcopd()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'list' object has no attribute 'deepcopd'
>>> d = a.deepcopy()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'list' object has no attribute 'deepcopy'
>>> 

```