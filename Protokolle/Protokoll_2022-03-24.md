# Pikos Python Kurs am 24.03.2022
Pad mit Notizen und Code:
https://md.ha.si/10163BrWQe29zGfcJ5-19g?edit

[TOC]

 

## Pikos Datei
```python
import turtle

class Dreieck:
    def __init__(self, laenge, farbe, fuellfarbe, strichdicke, winkelrundung, kaputt):
        self.laenge = laenge
        self.farbe = farbe
        self.fuellfarbe = fuellfarbe
        self.strichdicke = strichdicke
        self.winkelrundung = winkelrundung * laenge/2
        # spitzes dreieck: winkelrundung =0 => self.winkelrundung = 0 * laenge/2
        # halbrundes Dreieck: winkelrundung = 0.5 => self.winkelrundung = 0.5 * laenge/2
        # sehr rundes Dreieck: winkelrundung = 1 => self.winkelrundung = 1 * laenge/2

    def malen(self, farbe=None, fuellfarbe=None):
        if not farbe:  # Falsy  "falschig", Truthy "wahrig"
            farbe = self.farbe
        if not fuellfarbe:
            fuellfarbe = self.fuellfarbe
        t = turtle.Turtle()
        #stiftfarbe etc einstellen
        t.color(farbe, fuellfarbe)
        t.pensize(self.strichdicke)
        for i in range(3):
            t.fd(self.laenge - 2 * self.winkelrundung)
            # t.lt(120)  # mit spitzen Ecken
            t.circle(self.winkelrundung, extent=120)
            # circle(radius, extent, steps)
            # 0 = 0 => spitzes dreieck
            # 1 darf nicht länger sein als die halbe Laenge => Kreis

    def schrumpfen(self, ratio=0.5):
        # self.laenge = self.laenge * ratio
        self.laenge *= ratio

    def blinken(self, blink_anzahl=20):
        for i in range(blink_anzahl):
            self.malen()
            self.malen("white", "white")

adas_dreieck = Dreieck(400, "red", "blue", 3, 50, False)
adas_dreieck.blinken(10)
turtle.exitonclick()
```