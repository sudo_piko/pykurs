# Pikos Python Kurs 19.10.2021


## Abbildungen
https://cs.wellesley.edu/~cs110/lectures/L16/images/function.png  
https://chaos.social/system/media_attachments/files/107/128/641/933/924/594/original/79cedb55ceea7208.jpeg


## Mitschrift

@ alle: Terminumfrage wird morgen geschlossen, bitte eintragen, falls noch nicht geschehen:  

[https://nuudel.digitalcourage.de/oLfEgXQUoXiZRWiOWrKyh4CQ/admin](https://nuudel.digitalcourage.de/oLfEgXQUoXiZRWiOWrKyh4CQ/admin)  

  

Aufgaben vom 12.10.:  

**Aufgabe 1 - Sterne:**  

\- Tipp1 : erstmal basic Wissen zu Sternen(Geometrie)/Formen anschauen (Feststellung: bei ungerader Zahl an Zacken, sind Linien durchgehend)  

\- Tipp2: mit Stift und Papier aufschreiben/malen, was geplant ist.  

\- Drehungen müssen am Ende ein Vielfaches (Teiler?) von 360 Grad sein.  

\- Operatoren, um auf ungerade Zackenzahl zu kommen:  

    - "**%**": geteilt mit Rest, gibt den Rest als Ergebnis; Bsp.: "**% 2**" gibt den Rest bei Teilen durch 2 als Ergebnis (  

if zackenzahl %2\==1: \-->Überprüfung, ob die Zahl ungerade ist

(zackenzahl %2 ==0 wäre dann Test, ob Zahl gerade ist)  

    - "**//**": geteilt mit Rest, gibt das eigentlich Ergebnis aus  

winkel = (360/zackenzahl)\*(zackenzahl//2) \# Teilen mit Rest, Ergebnis

\- Konvention in python: Variablen in Kleinbuchstaben, Konstanten (ändern sich nicht innerhalb des Programms, am Anfang festgesetzt) in Großbuchstaben.  

Im Beispiel ist die Konstante:  ANZAHL\_STERNE = 4  

  

\- jeden Stern etwas heller als den vorherigen machen..???  

**\*\*\*\*\*\*\*\*\*Pikos Code \*\*\*\*\*\*\*\*\***  
from turtle import \*

ANZAHL\_STERNE = 4
zackenzahl = 7
color("red", "yellow")
begin\_fill()
for i in range(ANZAHL\_STERNE):
    if zackenzahl % 2 \==1: #Test, ob Zahl ungerade ist 
        winkel = (360/zackenzahl)\*(zackenzahl//2) #Teilen mit Rest, Ergebnis
        for j in range(zackenzahl):
            fd(100)
            lt(winkel)
    pu() \# Abstand zu vorherigem stern
    fd(200)
    pd()

end\_fill()

\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*  

**Aufgabe 2 & 3 - Matheprüfung**  

\- inputfrage = "was ist:" \+ str(rand1) + "plus" \+ str(rand2) --> string von den random-Zahlen gebildet.  

\- Punktestand: Wo muss dieser 0 sein? Wann steigt er? Zu Beginn festlegen punkte = 0. Wenn Eingabe richtig war: punkte = punkte +1 (Details s. im Code)  

Erweiterung: Wenn man sich nur um 1 verrechnet hat, soll das Programm einen darüber informieren, Ausgabe "Leider falsch!..."  

\- bei Abfrage, ob man um 1 daneben liegt: kann man mit Betrag machen, das ist abs()  

if abs(answer - (rand1+rand2))==1:

  

The abs() function returns the absolute value of the given number. If the number is a complex number, abs() returns its magnitude. (aus: [https://www.programiz.com/python-programming/methods/built-in/abs)](https://www.programiz.com/python-programming/methods/built-in/abs)  

**\*\*\*\*\*\*\*\*\*Pikos Code \*\*\*\*\*\*\*\*\***  

...folgt....  

  

  

**NEU:**  

Funktionsdefinitionen  

\- Ein Mal etwas definieren, das dann mehrmals durchlaufen kann. Pikos Beispiel: Dreiecke  

        def huebsches\_dreieck()\_  

            color....  

Oder:  

def hallo()  

    print("Nette Begrüßung")  

\- ganz wichtig: Funktion am Ende auch ausführen! Indem man hallo() oder huebsches\_dreieck() auch ins Skript schreibt. Durch die Definition an sich wird noch nichts ausgeführt  

  

\- ich kann auch Sachen in Funktionen reingeben, z.B. Argumente:  

def blauer\_strich(laenge): _jetzt wird eine Variable eingebracht die noch nicht definiert wurde._  

    pencolor("")  

    fd(laenge)  

  

\- können auch etwas zurück geben  

def summe(zahl1, zahl2):  

    s = zahl1 + zahl2  

    return s  

\--> return ist das, was bei Funktion rauskommt. Wenn Funktion hier ohne das return ausgeführt wird, wird s zwar berechnet, aber danach wieder "vergessen"  

Variable hinter return kann weitergegeben werden:  

ergebnis = summe(2,5)  

print(ergebnis)  

**print & return: Wichtige Unterscheidung/wie etwas zurück kommt:**  

\- **print**: wird auf der Konsole ausgegeben. Ist total sinnvoll, wenn ihr nicht wisst, warum etwas falsch ist oder als Hilfestellung/debuggen/spicken. z.B. print("Hier ist Zeile 80 und ich gebe den Typ von zahl1 aus:", type(zahl1))  

\- **return**: ... beendet die Funktion.  

\- **return vs. break**: return bricht aus Funktionen aus. break bricht aus Schleifen aus  

Bsp. return/Funktion beenden;  

    if type(zahl1) !== int:  

        return _\# Funktion hier beendet_  

    s = zahl ...  

  

**Wichtiger Unterschied**:**Funktionen und input**  

\- bei Funktionen tut ihr während es Programmirens etwas rein  

\- input wird beim Ausführen des Programms befüllt  

  

Tipp: Kommentare  

\- auch hilfreich zwischendurch: Kommentare. mit "**#**" Text wird ausgegraut dargestellt/ Rest der Zeile wird ignoriert  

  

input/output: dazu lustige Bilder:  

[https://chaos.social/system/media\_attachments/files/107/128/641/933/924/594/original/79cedb55ceea7208.jpeg](https://chaos.social/system/media_attachments/files/107/128/641/933/924/594/original/79cedb55ceea7208.jpeg)  

[https://de.toonpool.com/user/3652/files/geld\_drucken\_maschine\_euro\_eu\_po\_1664015.jpg](https://de.toonpool.com/user/3652/files/geld_drucken_maschine_euro_eu_po_1664015.jpg)  

  

  

**Veranstaltungshinweise:**  

\- Hamburg: geekfem Treffen am 21.10., 19:00  

\- nächste Woche voraussichtlich nochmal am Dienstag treffen, falls nicht andere Info über Mailingliste folgt




## Pikos Programme

### Hausaufgabenbesprechung
Siehe Ordner „Lösungen zu Aufgaben“


### Def

```
from turtle import *

def hallo():
    print("Hallo liebe Pythonistas! Schön dass Ihr da seid!")


def huebsches_dreieck():
    color("firebrick", "cornflowerblue")
    pensize(10)
    begin_fill()
    for i in range(3):
        fd(200)
        rt(120)
    end_fill()

def meine_lieblingsfunktion():
    huebsches_dreieck()
    hallo()
    print("3.1415926")
    if 5 == 5:
        print("Fünf ist fünf!")
        return 5

meine_fuenf = meine_lieblingsfunktion()
print(meine_fuenf)


def blauer_strich(laenge):
    pencolor("cornflowerblue")
    fd(laenge)

def hallo2(name):
    print("Hallo liebe " + name)

def summe(zahl1, zahl2):
    if type(zahl1) != int:
        print(" HIer ist Zeile 80 und ich gebe den Typ von zahl1 aus:", type(zahl1))
    s = zahl1 + zahl2 # Jetzt ist s die Summe der beiden Input-Zahlen
    print(s)
    return s



# zahl = 3
# zahlenstring = str(zahl)
#
# neuesding = print("hallo")
# print(neuesding)

meinesumme = summe(3, 5)

print(str(meinesumme) * 5)

```