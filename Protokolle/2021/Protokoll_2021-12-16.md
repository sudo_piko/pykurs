# Pikos Python Kurs am 18.11.2021

## Mitschrift 


## Pikos Datei
```python
pikos_fahrrad = {"marke":"Panther", "reifengroesse":28, "klingel":1, "bremsen":2, "besondere kennzeichen":["sticker", "korb", "kratzer"], "licht":True, "typ":"City-bike", "farbe":"blau", "besitzer*in":"Piko"}

kleingruppenfahrrad = {"marke": "Diamant", "reifengroesse": 28, "klingel": 2, "bremsen": 3, "besondere kennzeichen": ["glitzert  bei nacht", "klappert bei tag"], "licht": 2, "typ": "Hollandrad", "farbe": "schwarz", "besitzer*in": "elnaeju"}

clx_fahrrad = {"marke":"keine Ahnungr", "reifengroesse":28, "klingel":0, "bremsen":2, "besondere kennzeichen":["kann USB aufladen", "Hörnchen", "ungeputzt"], "licht":True, "typ":"Tourenrad", "farbe":"phosphor-grün", "besitzer*in":"clx"}

fahrradkeller = [pikos_fahrrad, kleingruppenfahrrad, clx_fahrrad]

# for wasistdasbloss in pikos_fahrrad:
#     print(wasistdasbloss)
#     print(pikos_fahrrad[wasistdasbloss])


# Hallo Piko! Dein blaues Fahrrad ist von der Marke Panther und hat ausreichend viele Bremsen.

def fahrradbewertung(fahrrad):
    """printe eine Bewertung für das eingegebene Fahrrad."""
    satzteil1 = "Hallo " + fahrrad["besitzer*in"] + "! "
    # satzteil2 = "Dein " + str(fahrrad.get("farbe")) + "es Fahrrad ist von der Marke " + fahrrad.get("marke") + " und"  # Version mit get
    satzteil2 = "Dein " +  fahrrad["farbe"]    + "es Fahrrad ist von der Marke " +   fahrrad["marke"]   + " und"

    if fahrrad.get("bremsen") == 0:
        bewertung = "nicht genügend"
    elif fahrrad.get("bremsen") < 2:
        bewertung = "eine für Feldwege geeignete Anzahl"
    else:
        bewertung = "ausreichend viele"

    satzteil3 = " hat " + bewertung + " Bremsen."

    satz = satzteil1 + satzteil2 + satzteil3
    print(satz)



# for cooles_bike in fahrradkeller:
#     fahrradbewertung(cooles_bike)



gedicht = """The Python
by Joseph Hilaire Pierre Belloc
A Python I should not advise, —
It needs a doctor for its eyes,
And has the measles yearly.
However, if you feel inclined
To get one (to improve your mind,
And not from fashion merely),
Allow no music near its cage;
And when it flies into a rage
Chastise it, most severely.
I had an Aunt in Yucatan
Who bought a Python from a man
And kept it for a pet.
She died, because she never knew
These simple little rules and few; —
The snake is living yet."""

# print(gedicht.lower())

buchstabendictionary = {"e":0}

for buchstabe in gedicht.lower():
    if buchstabe == "e":
        buchstabendictionary[buchstabe] += 1

print(buchstabendictionary["e"])
```
