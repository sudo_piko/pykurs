# Pikos Python Kurs am 18.11.2021


##  Protokoll: Balub

[TOC]

### Organisatorisches

#### Links:

* Gitlab (Aufgaben und Protokolle)
  https://gitlab.com/sudo_piko/pykurs

* Mailingliste
  https://lists.haecksen.org/listinfo/python

* Doodle für Extrastunde (für alle denen der Kurs zu schnell geht und die nochmal eine zusammengefasste Runde Theorie brauchen)
  https://nuudel.digitalcourage.de/mkhkP1eEJbPapQgKxnflq79B/admin

* Kontakt zu Piko: Python@ithea.de

* Gruppen-Pad: https://md.darmstadt.ccc.de/uUxnBOlqS8eKN09CfTd3hQ?view# gerne zur Selbstorganisation nutzen!

#### Termine

- Am 2.12. ist piko nicht da. Entweder gibt es eine Vertretung oder wir treffen uns zum gemeinsamen programmieren, spielen oder andere $Dinge

#### Projekt

- In Zukunft werden wir als Kurs an einem Projekt arbeiten. Die Abstimmung hat ergeben, dass wir ein Quiz machen wollen. Die künftigen Hausaufgaben werden also ein bisschen darauf vorbereiten. 
  - Wer Ideen / Codefragmente / Code / Tipps / Sonstiges für ein Quiz hat, kann das aber gerne auch vorher schon teilen :) 



### Python

#### Tabs vs. Spaces

<u>Warum Spaces:</u> Code, den man von [Stackoverflow](https://stackoverflow.com/) kopiert, hat automatisch Leerzeichen als Einrückung. Das ist leichter in ein eigenes Projekt zu integrieren, wenn man dort auch Leerzeichen verwendet

#### Funktionen

##### Default-Argumente

- können verwendet werden, um es zu ermöglichen Werte zuzuweisen, die beim Aufruf nicht mitgegeben werden müssen (aber können!)

- PyCharm gibt einem im Tooltip Hinweise zum Funktionsaufruf

- Werte zu Default-Argumenten werden ohne Leerzeichen zugewiesen (stilistisch grenzt das von anderen Zuweisungen ab. Wenn man doch mal Leerzeichen drin hat, ist es aber kein Fehler)

- Beispiel: 

  ```python
  # Funktion mit Default-Argument Seitenlänge
  def n_eck(ecken, seitenlaenge=150, farbe="rot"):
      pencolor(farbe)
  	for seiten in range(ecken):
  		fd(seitenlaenge)
  		lt(360/ecken)
   
  
  # kann auf folgende Arten aufgerufen werden:
  
  n_eck(5) #seitenlänge wird wegen default-Argument implizit auf 150 gesetzt, farbe bleibt rot
  
  n_eck(5, 30) #seitenlänge wird 30
  
  n_eck(5, 20, "blau") #5 Ecken, 20 als Seitenlänge und blaue Farbe. Das geht, weil die Reihenfolge der Argumente gleich ist 
  
  n_eck(seitenlaenge=33, ecken=2) # geht auch. Seitenlänge ist dann 33, ecken gibt es 2 und Farbe bleibt Default rot
  
  
  ```

##### Scopes und Namespaces

- **Scope** = Gültigkeitsbereich, Bereich

- Variablen haben Scopes in denen sie gültig sind. Nur in ihrem Scope oder in inneren Scopes versteht Python, was mit dem Variablennamen gemeint ist.

- In Funktionen (und for-Schleifen) leben Variablen, die von außerhalb nicht aufgerufen werden können:

  ```python
  def funktion():
      a = 3
      return "Hallo"
  
  funktion() #Hallo
  print(a) #Fehler: NameError: name 'a' is not defined. Weil a nur in der Funktion bekannt ist
  ```

- Andersrum kann es funktionieren, weil Python von Innen nach Außen auswertet und versucht, die Variable zu finden:

  ```python
  mensch1 = "Andrea"
  mensch2 = "Peter"
  
  def function():
      print(mensch1) #python sucht in der Funktion, wer mensch1 ist. Findet nichts, also wird eine Ebene drüber geguckt. Da ist mensch1 = "Andrea". Also wird Andrea ausgegeben.
      mensch2 = "Idefix" #hier wird Python in der Funktion ein zweiter Mensch vorgestellt. 
      print(mensch2) #also wird hier Idefix ausgegeben. Nach mensch2 global wird nicht geschaut, weil wir in der Funktion sind. Also wird die globale Variable nicht geändert!
      
  print(mensch1) #Andrea
  print(mensch2) #Peter, weil in tiefere Ebenen nicht geschaut werden kann. Werte in der Funktion sind hier unsichtbar
  function()
  print(mensch2) #Peter
    
  ```

- Wenn man in der Funktion eine Variable einer höheren Ebene verändern will geht das mit dem *global* Stichwort (Variable muss in der höheren Ebene bekannt sein!):

  ```python
  b = 1
  
  def function():
      global b
      b += 2
  
  function()
  print(b) #3
  ```

- Ebenenhierarchie von Scopes: 
  - global (ganze Programmdatei)
  - enclosing (Funktionen, die andere Funktionen einschließen)
  - local (Funktionen, tiefste Ebene)
- **Namespaces** wird als Begriff manchmal synonym zu Scopes verwendet. Meint aber eigentlich die Gültigkeitsbereiche von Modulen. Also Programmteile, die man über imports hereinholt (wie wir das bei turtle, math oder random schon kennengelernt haben)
- Ebenenhierarchie von Namespaces:
  - built-in (ganz Python)
  - global (ganze Programmdatei)
  - local (kleinstes. Also in Funktionen, oder Schleifen)

- Piko hat tolle Grafiken zu den Ebenen und fügt die sicher hier noch ein

  

#### Sonstiges Spannendes am Rande zum selber Vertiefen

- Funktionsaufrufe und andere Ausdrücke werden von innen nach außen ausgewertet 

  ```python
  def f(a):
      a += 2
      return a
  
  f(2+4) #rechnet erst 2+4. Ruft dann f(6) auf
  ```

- <u>f-Strings</u> machen glücklich! (sagt Piko (und hat Recht!)) Werden aber erst später behandelt. Damit kann man Strings schön formatieren und Variablen einsetzen ohne gekomma oder geplusse mit anführungszeichen-krampf

  ```python
  zahl = 15
  name = "Python-Kurs"
  
  print(f'Heute waren ca. {zahl} Begeisterte beim {name}')
  ```

- Man kann Funktionen in Variablen speichern!

  ```python
  def sage_hallo(name):
      print("Hallo", name)
    
  funktion = sage_hallo
  funktion("Mensch") #Hallo Mensch
  ```

  

## Pad

### Hausaufgabe 1
~~~
def n_eck(ecken, seitenlaenge=150):
    for seiten in range(ecken):
        fd(seitenlaenge)
        lt(360/ecken)
        
n_eck(5)
n_eck(5, 200)
n_eck(seitenlaenge=2, ecken=20)

        
        
def buntes_n_eck(stiftfarbe, fuellfarbe, ecken, laenge=150, stiftdicke=5):
    color(stiftfarbe, fuellfarbe)
    pensize(stiftdicke)
    begin_fill()
    for seiten in range(ecken):
        fd(laenge)
        lt(360/ecken)
    end_fill()
    
buntes_n_eck("firebrick", "purple", 5, 100, 10)
buntes_n_eck("firebrick", "purple", 4, 400)
buntes_n_eck("firebrick", "purple", stiftdicke=20)
~~~
### Leerzeichen bei "="
~~~
var1 = 50

if var1 == 51:
~~~


## Pikos Datei

Hausaufgabe 2:  
Scopes!
~~~
x = 0
y = 0
def incr(x):
    y = x + 1
    return y
# incr(5)
# print(x, y)
~~~

https://realpython.com/python-scope-legb-rule/


Namespaces!  
~~~
import math

print(math.sqrt(5))

print(type(math.pi))
~~~


## Links zu Abbildungen
### Scopes als konzentrische Kreise:  
https://res.cloudinary.com/dyd911kmh/image/upload/f_auto,q_auto:best/v1588956604/Scope_fbrzcw.png  
(Erklärung hier: https://www.datacamp.com/community/tutorials/scope-of-variables-python )  

### Am Code-Beispiel
https://miro.medium.com/max/864/1*a2rQOJ6Ji2zghXVhYipHJQ.png  
(Erklärung hier: https://medium.com/@gough.cory/understanding-python-namespaces-through-recursion-c921cbd4e522 )  

## Auch in for-Schleifen
https://www.askpython.com/wp-content/uploads/2019/08/python-namespace-example.png.webp  
(Erklärung hier: https://www.askpython.com/python/python-namespace-variable-scope-resolution-legb )  

## Namespaces: für Module
https://data-flair.training/blogs/wp-content/uploads/sites/2/2018/01/Namespaces-1.png
(Erklärung hier: https://data-flair.training/blogs/python-namespace-and-variable-scope/