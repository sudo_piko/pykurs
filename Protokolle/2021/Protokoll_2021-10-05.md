# Pikos Python Kurs 05.10.2021

### Aufgabe 1
```
antwort = input("Was ist drei mal drei? ") #input wartet auf eine Eingabe, muss man machen sonst läuft nix weiter
print("Das hier war deine Antwort:") #hier fehlte ein Anführungszeichen
print(antwort)
print("Die Variable 'antwort' hat folgenden Typ:", type(antwort)) #hier fehlte eine Klammer, der Typ der Antwort ist immer str (liegt am input?)
for buchstabe in antwort: # die for-Schleife ist schlau, hier bekommt sie einen string und nimmt jeden einzelnen Buchstaben, wenn ich sie eine Liste bekommt nimmt sie jedes item der Liste
    print(buchstabe*3)
if len(antwort) < 2: #hier fehlte der :
    print("Das ist aber eine knappe Antwort!")
elif len(antwort) < 10: #hier ist elif besser, damit bei einem Wert < 2 nicht auch noch der < 10 Wert geprintet wird
    print("Das ist eine Antwort mit einer guten Länge!")
else:  #das hier ist der Auffänger für die beiden oberen
    print("Mir ist die Antwort zu lang!")
if antwort == "neun" : #hier fehlte ein zusätzliches = (das ist eine Python-Besonderheit bei Vergleichen), das hier ist eine zusätzliche prüfung, die unabhängig von den oberen durchlaufen wird
    print("Deine Antwort war sogar richtig!")

print(buchstabe) # die Variable Buchstabe bleibt auch außerhalb der for Schleife bestehen
```

### Organisatorisches
Alle Ressourcen unter: https://gitlab.com/sudo_piko/pykurs
Pycharm-Umfrage: fast alle haben eine IDE, größtenteils Pycharm.
Hausaufgaben waren machbar für fast alle

### zu Input
Mit input kann man eine Eingabe vom Benutzer des Programmes abfragen.
Bei Input kommt immer ein string heraus, selbst wenn jmd. Zahlen eintippt!

### datetime und Fußballspielen
Ein neues kleines Programm, was macht das?
Scheint etwas wie ein Zeit-Zählprogramm für Fußballspiele zu sein
datetime.now() - eine Funktion die immer die aktuelle Zeit ausgibt
abpfiffzeit wird ausgerechnet, dann
 eine While True Schleife (eine Schleife die nie endet)
und, wenn die abpfiffzeit erreicht ist, ein break (das bricht/stoppt eine Schleife)

..das ist ein Beispiel für eine fußgesteuerte Scheife (der Kopf lässt die Schleife ewig laufen, das break zum Schluss sorgt für das Ende)
Kopfgesteuerte Schleifen hatten wir beim letzten Mal schon, da steht neben dem While schon wann sie endet.

Bei einer fußgesteuerten Schleife wird der "Körper" min. 1x ausgeführt, erst dann wird gestoppt.
Eine Kopfgesteuerte Schleife endet ggfs. schon oben, vor dem ersten Ausführen.

### Aufgabe 2 - AND und OR
Auf https://de.wikipedia.org/wiki/Wahrheitstabelle kann man einer Wahrheitstabelle beim entstehen zusehen.
Das V ist ein inklusives(!) oder, der Strich mit dem Haken ist ein "nicht" oder "Gegenteil"

A and B - Es ist Nacht und es ist dunkel (es muss beides erfüllt sein)
A or B - Ist toleranter (auch hier ein inklusives oder), es reicht wenn es Nacht ist oder wenn es dunkel ist (nur eins muss erfüllt sein)

Wer mehr Lust auf formale Logik hat darf sich gerne bei Piko melden =)

### Aufgabe 3 Pycharm
Poll: Gibt es noch Fragen zu Pycharm? - es wird ein bisschen allgemeine Einführung gewünscht.
Wozu sollte man Code in mehrere Blätter aufteilen? - Um bei viel Code die Übersicht zu behalten
Tabs sind für verschiedene Dateien, kann man auch mit Firefox-Shortcuts wechseln und bedienen.
Pycharm wird auch nochmal im Anschluss Thema

### Module und Libraries
Das import statement kennen wir schon von der turtle.
Sachen die man importieren kann heißen in Python "Module", wie z.B. import datetime (vorhin beim Fußballprogramm), oder auch random (Zufallsfunktionen, mit denen man viel Spaß haben kann).
Wenn ich mit "random import *" arbeite, kann ich die funktionen von random auch direkt nutzen. Das kann aber zu Verwirrung führen.
statt random.shuffle kann ich dann aber bequemerweise auch direkt mit shuffle arbeiten.

Das Internet hält zahlreiche Module bereit die man sich herunterladen kann! Module kann man sich auch selbst schreiben.
In der nicht-Python-Welt heißen diese Module oft libraries.
Man kann auch viele Module gleichzeitig aktiv haben. Wenn ich dann aber zwei Module mit import * aktiv habe, dann kann es Probleme geben wenn beide dieselben Funktionsnamen verwenden.
Deshalb macht man das mit dem import * eigentlich nicht, sondern schreibt den Modulnamen davor.

### Termine
Dieses Wochenende ist Haecksen-Geekend, den Fahrplan fidet ihr unter https://events.haecksen.org/#event
Da gibt es einen git-Workshop, das wird spannend für uns. Git kann man als Zeitreise beim eigenen Entwickeln verwenden und zusammen zum coden verwenden.

# Pikos Textdatei

```
antwort = input("Was ist drei mal drei? ")

print("Das hier war deine Antwort:")
print(antwort)
print("Die Variable 'antwort' hat folgenden Typ:", type(antwort))

for buchstabe in antwort:
    print(buchstabe*3)

if len(antwort) < 2:
    print("Das ist aber eine knappe Antwort!")
    wort = "kurz"
elif len(antwort) < 10:
    print("Das ist eine Antwort mit einer guten Länge!")
    wort = "ok"
else:
    print("Mir ist die Antwort zu lang!")
    wort = "lang"


if antwort == "neun":
    print("Deine Antwort war sogar richtig!")
```
**************************************

Kleines fiktives Programm mit fußgesteuerter Schleife
```
import datetime

anstoßzeit = datetime.now()
abpfiffzeit = anstoßzeit + datetime.timedelta(0, 0, 0, 0, 45)
while True:
    leite_spiel()
    if datetime.now() >= abpfiffzeit:
        break
abpfiff()
```

Kopfgesteuerten Schleife
```
x = 10
while x < 10:
	...
```


Fußgesteuerten Schleife
```
while True:
	...
	if x >= 10:
		break
```


### Aussagenlogik
¬ = „nicht“; in Python: not
∨ = lat ‚vel‘ = oder; in Python: or
∧ = und / „geschnitten“; in Python: and

# Pikos Python-Shell
Zum Thema „Module importieren“
```
>>> import turtle
>>> turtle.fd()
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: fd() missing 1 required positional argument: 'distance'
>>> turtle.fd(100) 
>>> import datetime
>>> datetime.time()     
datetime.time(0, 0)
>>> datetime.datetime.now()
datetime.datetime(2021, 10, 5, 20, 25, 30, 549267)
>>> import random
>>> random.randint(10)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: randint() missing 1 required positional argument: 'b'
>>> random.randint(10,20)
15
>>> random.randint(10,20)
20
>>> random.randint(10,20)
12
>>> random.randint(10,20)
18
>>> random.randint(10,20)
20
>>> random.randint(10,20)
20
>>> random.randint(10,20)
18
>>> random.randint(10,20)
16
>>> random.randint(10,20)
18
>>> random.randint(10,20)
14
>>> random.choice(["apfel", "birne", "kiwi"])
'apfel'
>>> random.choice(["apfel", "birne", "kiwi"])
'apfel'
>>> random.choice(["apfel", "birne", "kiwi"])
'apfel'
>>> random.choice(["apfel", "birne", "kiwi"])
'kiwi'
>>> liste = ["apfel", "birne", "kiwi", "ananas", "banane", "feige", "granatapfel", "quitte"]
>>> liste
['apfel', 'birne', 'kiwi', 'ananas', 'banane', 'feige', 'granatapfel', 'quitte']
>>> random.shuffle(liste)
>>> liste
['apfel', 'kiwi', 'quitte', 'feige', 'ananas', 'granatapfel', 'banane', 'birne']
>>> from random import *
>>> shuffle(liste)
>>> liste
['banane', 'quitte', 'ananas', 'granatapfel', 'feige', 'birne', 'apfel', 'kiwi']
>>> import random
>>> random.shuffle(liste)
>>> from turtle import *
>>> fd(100)
>>> from piko import *
```
