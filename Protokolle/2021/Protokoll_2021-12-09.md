# Pikos Python Kurs am 18.11.2021

## Mitschrift naerrin
Hausaufgaben besprochen

### Aufgabe 1 Dictionaries
#### 1.1 get()

get gibt Inhalt des Keys zurück, FALLS der Key da ist. Sonst None. Dabei bricht es aber das Programm nicht ab.

get ist eine Funktion, genauer: eine Methode von Dictionaries, wie append eine Methode von listen ist.

Anwendungsoption:
```python
keller = {"fahrrad":8,       "lastenrad":3,      "roller":7,      "rollschuhe":3,      "skateboards":3}

anzahl_hoverboards = keller.get("hoverboards", 0) #gibt 0 Hoverboards aus, wenn print

```

Eine For-Schleife durch ein Dictionairy holt nur die Keys, die values können direkt angesprochen werden:
```python

for ding in keller:
    print(ding)  #keys
    print(keller[ding])  #values

```

andere Option values abzufragen:
```python

for wert in keller.values():
    print(wert)

```


+= 1 ist die verkürzte Form von
zahl = zahl + 1
damit kann also der vorhergende Wert um n erhöht werden
+= 1 heißt also, erhöhe diese variable um 1


Kellerinventar erweitern:

keller["lastenrad"] = keller.get("lastenrad", 0) + 1erhö
Oder
keller[monster] = 1 # setzt die anzahl an monster im keller auf 1, egal ob vorher 7 drin waren oder keins.


#### 1.2 Aufsummieren

Werte zusammenrechnen:
```python
sum(keller.values())

```


moshs lösung:
```python
fahrzeuge=0
keller = {"fahrrad":8, "lastenrad":3, "roller":7, "rollschuhe":3, "skateboards":3}

for dingsi in keller:
print('dingsi: ' +dingsi)
print(keller[dingsi])
fahrzeuge+=keller[dingsi]

print('So viele Fahrzeuge haben wir: ' + str(fahrzeuge))

```


Julikas Lösung:
```python
keller = {"fahrrad":8, "lastenrad":3, "roller":7, "rollschuhe":3, "skateboards":3}
kellerinventur = 0
for ding in keller:
    kellerinventur += keller.get(ding)

print(kellerinventur)


```

dictionaries sind so, dass wir von den keys auf die values kommen, andersrum ist das eher selten der fall
```python

keller["lastenrad"]
keller.get("lastenrad")
```
Wir wollen hier immer values rausfinden


### 2. Quiz weitermachen

Quizprobieren.py

Gegeben ist dictionary mit Fragenkatalog
```python
fragen =  {"3+5":"8", "Tier?":"nein"}

def fragestellen(frage, richtige_antwort):
    gegebene_antwort = input(frage + " ")
    if gegebene_antwort == richtige_antwort:
        print("Richtig!")
        return True
    else:
        print("Falsch!")
        return False

for frage in fragen:
    fragestellen(frage, fragen[frage])


```


## Pikos Datei
```python
keller = {"fahrrad":8, "lastenrad":3, "roller":7, "rollschuhe":3, "skateboards":3}

anzahl_hoverboards = keller.get("hoverboards", 0)

for ding in keller:
    print(ding)
    print(keller[ding])

    
# die Methoden values() und keys() greifen auf die values und keys des dictionaries zu.
print(type(keller.values()))

for wert in keller.values():
    print(wert)
    
# gleichwertig mit:
for ding in keller:
    print(keller[ding])

    
    
# neues Fahrrad:
keller["fahrrad"] = 9
keller["skateboards"] += 1  # keller[...] = keller[...] + 1


# kurzer Blick auf +=
zahl = 1 + 5
dashierwirderstspaeterangeschaut = zahl + 1
zahl = 7
zahl = zahl + 1
zahl += 1



# Wenn wir nicht wissen, ob schon Lastenräder im keller stehen:
keller["lastenrad"] = keller.get("lastenrad", 0) + 1

# neues Ding:
keller["monster"] = 1  # setzt genau auf 1
keller["monster"] = keller.get("monster", 0) + 1


# Wenn wir wissen, dass der key "lastenrad" im Keller vorhanden ist, dann ist folgendes äquivalent:
zahl1 = keller["lastenrad"]
zahl2 = keller.get("lastenrad")
```

### Aufgabe 2
```python
fragen = {"3+5":"8", "Tier?":"nein"}

def fragestellen(frage, richtige_antwort):
    gegebene_antwort = input(frage + " ")
    if gegebene_antwort == richtige_antwort:
        print("Richtig!")
        return True
    else:
        print("Falsch!")
        return False

for frage in fragen:  # wie oben: "for ding in keller" geht die Vehikel-Namen – also die keys – durch
    fragestellen(frage, fragen[frage])  # frage ist dann ein key, fragen[frage] ist dictionary[key], also wie keller[ding] => keller["fahrrad"]


```
