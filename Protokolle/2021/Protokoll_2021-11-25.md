# Pikos Python Kurs am 18.11.2021


## Protokoll: waldwesen

## Organisatorisches

### Links:

* Gitlab (Aufgaben und Protokolle)
  https://gitlab.com/sudo_piko/pykurs

* Mailingliste
  https://lists.haecksen.org/listinfo/python

* Kontakt zu Piko: Python@ithea.de

* Gruppen-Pad: https://md.darmstadt.ccc.de/uUxnBOlqS8eKN09CfTd3hQ?view# gerne zur Selbstorganisation nutzen!

### Termine

- Am 2.12. ist piko nicht da. Wir treffen uns zum gemeinsamen programmieren, es wird drei Aufgaben von Piko geben, die wir gemeinsam angehen können.
Oder wir denken uns zusätzliche Aufgaben aus. Wie wir wollen :)
- * Sa, 27.11. 11:00: spontanes Haecksenfrühstück in Ada (https://meeten.statt-drosseln.de/b/cri-uye-ogv-vfh) auch noch-nicht-Haecksen sind herzlich willkommen!

## Hausaufgaben
Hausaufgabenbesprechungspad: https://md.ha.si/k7xV8arGQr-MuujPirO6BA?both

### Aufgabe 1 Fragen sammeln
siehe Pad

### Aufgabe 2
#### Lösungen der Teilnehmenden, ein paar Kommentare von waldwesen

#### Variante 1
```
def fragestellen(frage, richtige_antwort):
    #hier steht eigentlich noch ein Docstring :D
    gegebene_antwort = input(frage)
    if gegebene_antwort.lower().replace(" ", "") in [x.lower().replace(" ", "") for x in richtige_antwort]:
    # guckt nach ob die Antwort in der Liste der richtigen Antworten enthalten ist, macht aus der gegebenen Antowort und der Liste der richtigen Antworten kleine Buchstaben und entfernt die Leerzeichen das ist sehr elegant und muss noch nicht jede/r verstehen!
        print("Richtig! Sehr gut!")
        return True
    return False
```

#### Variante 2

```
from datetime import date #tolles Pythonmodul, gerne mit beschäftigen!
import calendar
heute = date.today()
print(calendar.day_name[heute.weekday()])

def fragestellen():
    print("Hier musst du den Wochentag eingeben!") #erstmal Frage ankündigen
    eingabe = input('Wochentag: ')
    if eingabe == heute: #"heute" ist eine golbale Variable
        print('Super! Richtig!')
    else:
        print('Sorry! Falsch! Das war nix!')

fragestellen()
```

#### Variante 3

```
def fragestellen(frage, richtige_antwort): # eigentlich sollten die Argumente anders benannt werden als die Variablen unten.
    """überprüft eine Nutzereingabe auf ihre Richtigkeit"""
    antwort = input(frage)  # Frage stellen und Antwort speichern
    if antwort == richtige_antwort:    # Überprüfen, ob die Antwort richtig ist
        print("Bravo! Das ist richtig.")  # Wenn ja: Ein Lob printen und True zurückgeben
        return True # Anmerkung: Die Reihenfolge ist hier wichtig, das Print-Statement muss zuerst dastehen. Wieso? -> weil return die Funktion beendet
    else:
        print("Leider falsch.")
        return False # Wenn nein: False zurückgeben.

frage = "Wie heißt die Sängerin Taylor mit Nachnamen?" # hier kann nun jede beliebige Frage/richtige Antwort-Kombi eingetragen werden
richtige_antwort = "Swift"

fragestellen(frage, richtige_antwort) # Funktion ausführen nicht vergessen!
```


#### Variante 4

```
def fragestellen(frage="What is the answer to the universe and everything? ", richtige_antwort="42"):

    """stellt eine Frage und zählt Punkte mit,
       printet dann diese Punkte sowie einen Hinweis, ob die Antwort (= input) richtig/falsch war.
       (inkl. default-Frage mit der Antwort: "42", wenn "feststellen()" keine Argumente mitgegeben werden) """
    antwort = input(frage)
    punkte = 0 #Punkte sollten hier noch nicht gezählt werden, da diese Funktion nur zum Fragestellen gedacht ist. Bei mehrfacher Auführung würde man hier die Punkte immer wieder auf 0 setzen.
    if antwort == richtige_antwort:  # todo: beides erlauben? "42" und "zweiundvierzig"?
        punkte = punkte +1
        print("Gratuliere! ( Punkte: ", punkte, ")")
        return True
    else:
        print("sorry, falsche antwort! (Punkte: ", punkte, ")")
        return False
```


#### Variante 5 - Sehr nah an einer Musterlösung :)

```
fragestellen()

def fragestellen(frage, richtige_antwort):
    """Stelle Quizfrage, überprüfe Antwort an Vorgabe, printe Feedback und gib aus, ob richtig oder falsch """
    gegebene_antwort = input(frage) #Hier muss irgendwie die Frage gestellt werden")
    if str(gegebene_antwort) == str(richtige_antwort): #Überprüfen, ob die Antwort richtig ist
        print("Das war richtig! Toll gemacht!")
        return True #Wenn ja: Ein Lob printen und True zurückgeben
    else:
        print("Ne, neee, so nicht.")
        return False #Wenn nein: False zurückgeben.
```

### Aufgabe 3 Schöne End-Ausgabe
Sagen wir, wir haben alle Quizfragen beantwortet. Es waren anzahl_fragen Fragen, wir haben punkte viele richtige Antworten gegeben.
Definiert eine Funktion, die keinen Wert zurückgibt, sondern nur printet. Sie soll angemessen loben oder ermutigen.
Schreibt diese Funktion! Sie sollte mindestens anzahl_fragen und punkte als Argumente übernehmen, vielleicht gibt es noch weitere Dinge.
Denkt darüber nach, was diese Ausgabe so alles an Text beinhalten könnte. Sammelt Ideen, auch wenn Ihr Euch nicht denken könnt, wie Ihr sie in Python gießen könnt; macht gerne eine längere Wunschliste.

* Wünsche siehe Pad

#### Beispielimplementierung von Piko

```
def endausgabe (anzahl_fragen, punkte, name)
    print ("Das waren alle Fragen")
    if (anzahl_fragen == punkte): #wichtig: das erste was hier drin wahr ist, wird ausgeführt, d.h. bei 100/100 Punkten ist das erste wahr, bei 75 das erste nicht, das zweite schon
        print ("Du hast alle Fragen richtig beantwortet. Glückwunsch!"
    elif punkte >= 0.7 * anzahl_fragen
        print ("Ganz gut")
    elif punkte >= 0.5 * anzahl_fragen
        print ("Grade noch gut gegangen")
    else:
        print ("Naja")
```

## Neues Thema: Dictionaries

* Brauchen wir sehr wahrscheinlich für Quiz-Antworten :)
* erkennt man an geschweiften Klammern {}
* Beispiel: ich möchte Dinge in meinem Keller zählen
```
keller = {"fahrrad":8, "roller":3, "rollschuhe":7}`
```
* wenn man das vollständig printen will, braucht es:
```
for ding in keller:
    print(ding)
    print(keller[ding])
```
 ...ein bisschen wie eine Liste.
* Der erste Wert pro Eintrag im Dictinary ist der Key (hier z.B. fahrrad), mit einem Doppelpunkt davon getrennt ist der zugehörige value (hier 8).
* Praktische Dictionary Funktionen:
    * print(keller.keys()) Die Funktion keys wird über den Punkt für das keller-Dictionary aufgerufen, gibt alle Keys aus
    * print(keller.values()) Die Funktion values wird über den Punkt für das keller-Dictionary aufgerufen, gibt alle Values aus
* Was mache ich wenn ich ein neues Fahrrad kaufe?
    
    `keller["fahrrad"] = keller["fahrrad"] +1`

    oder kürzer: 
    
    `keller["fahrrad"] += 1`

* Was wenn ein Monster in den Keller einzieht?
   
    `keller["monster"] = 1`

Siehe auch:   
https://dasmaichen.de/games/wodewick.html

# Hausaufgabenbesprechung im Pad
https://md.ha.si/k7xV8arGQr-MuujPirO6BA#

## Aufgabe 1 – Fragen und Antworten
"Was ist eine gute Zufallszahl?" "4"
("Welche Sprache lernst du hier?", ["python", "python3"])
'wie heißt piko','piko'

frage = "What is the answer to the universe and everything? "
richtige_antwort = "42"

frage_1 = "Wie viele Stunden hat ein Tag?"
antwort_1 = "24"

*Was machen Pandas im Winter? ['schlafen', 'essen', 'sich im Schnee waelzen']

* Welcher muslimische Herrscher besiegte die Kreuzfahrerheere 1187 an den Hörnern von Hattin?
    * Sultan Saladin (auch richtig: Saladin, Salah ad-Din)
    * Bonuspunkte für: Salah ad-Din Yusuf ibn Ayyub ad-Dawīnī
    
"Wie heißt der beste Freund von Calvin?"" "Hobbes"


	print("Was haben Igel, Murmeltiere und Fledermäuse gemeinsam?")
	print()
	print("Winterschlaf, Pandarolle, scheinen, schmelzen")
    

Wie heißt die berühmte Sängerin Taylor mit Nachnamen? a) Spears b) Swiffer c) Swift d) Manson
Was ist Ascorbinsäure? a) Vitamin C b) Aspirin c) Desinfektionsmittel d) Anti-Aging-Mittel


## Aufgabe 2 – Funktion

~~~
def fragestellen(frage, richtige_antwort):
    #hier steht eigentlich noch ein Docstring :D
    gegebene_antwort = input(frage)
    if gegebene_antwort.lower().replace(" ", "") in [x.lower().replace(" ", "") for x in richtige_antwort]:
        print("Richtig! Sehr gut!")
        return True
    return False


if "python3" in ["python", "python3"]:
    ...



from datetime import date
import calendar
heute = date.today()
print(calendar.day_name[heute.weekday()])

def fragestellen():
    print("Hier musst du den Wochentag eingeben!")
    eingabe = input('Wochentag: ')
    if eingabe == heute:
        print('Super! Richtig!')
    else:
        print('Sorry! Falsch! Das war nix!')

fragestellen()





def fragestellen(frage, richtige_antwort):
    """überprüft eine Nutzereingabe auf ihre Richtigkeit""" 
    antwort = input(frage)  # Frage stellen und Antwort speichern
    if antwort == richtige_antwort:    # Überprüfen, ob die Antwort richtig ist
        print("Bravo! Das ist richtig.")  # Wenn ja: Ein Lob printen und True zurückgeben
        return True # Anmerkung: Die Reihenfolge ist hier wichtig, das Print-Statement muss zuerst dastehen. Wieso?
    else:
        print("Leider falsch.")
        return False # Wenn nein: False zurückgeben.

frage = "Wie heißt die Sängerin Taylor mit Nachnamen?" # hier kann nun jede beliebige Frage/richtige Antwort-Kombi eingetragen werden
richtige_antwort = "Swift"

fragestellen(frage, richtige_antwort) # Funktion ausführen nicht vergessen!



def quadrat(x):
    return x * x
    print("Ich habe den Fisch gestohlen")  # Wird nichtmehr ausgeführt.
    weltuntergang("jetzt")
    ...



def fragestellen(frage="What is the answer to the universe and everything? ", richtige_antwort="42"):

    """stellt eine Frage und zählt Punkte mit, 
       printet dann diese Punkte sowie einen Hinweis, ob die Antwort (= input) richtig/falsch war.
       (inkl. default-Frage mit der Antwort: "42", wenn "feststellen()" keine Argumente mitgegeben werden) """
    antwort = input(frage)
    punkte = 0
    if antwort == richtige_antwort:  # todo: beides erlauben? "42" und "zweiundvierzig"? 
        punkte = punkte +1 
        print("Gratuliere! ( Punkte: ", punkte, ")")
        return True
    else:
        print("sorry, falsche antwort! (Punkte: ", punkte, ")")
        return False

fragestellen()


def fragestellen(frage, richtige_antwort):
    """Stelle Quizfrage, überprüfe Antwort an Vorgabe, printe Feedback und gib aus, ob richtg oder falsch """
    gegebene_antwort = input(frage) #Hier muss irgendwie die Frage gestellt werden")
    if str(gegebene_antwort) == str(richtige_antwort): #Überprüfen, ob die Antwort richtig ist
        print("Das war richtig! Toll gemacht!")
        return True #Wenn ja: Ein Lob printen und True zurückgeben
    else:
        print("Ne, neee, so nicht.")
        return False #Wenn nein: False zurückgeben.
~~~


## Aufgabe 3 –  Wunschliste für Ausgabetext


* zum Dank eine turtle malen :), je mehr punkte desto bunter
* gemessene Zeit angeben
* je nach geschafften Prozent malt die Turtle ein Eis oder eine Fete 
* Medallien
* Punkte je Kategorie
* Lob je Schwierigkeitsstufe
* kleine Fanfare oder Applaus (also Tonausgabe) bei 100%
* Bei 100% eine Bonusfrage
* Auflösung der falschen Antworten



# Pikos Datei

## Hausaufgabe 3: Endausgabe

Aufgabenstellung:  
Sagen wir, wir haben alle Quizfragen beantwortet. Es waren anzahl_fragen Fragen, wir haben punkte viele richtige Antworten gegeben.
Definiert eine Funktion, die keinen Wert zurückgibt, sondern nur printet. Sie soll angemessen loben oder ermutigen.
Schreibt diese Funktion! Sie sollte mindestens anzahl_fragen und punkte als Argumente übernehmen, vielleicht gibt es noch weitere Dinge.

~~~
def endausgabe(anzahl_fragen, punkte, name):
    print("Das waren alle Fragen.")
    if punkte == anzahl_fragen:
        print("Du hast alle Fragen richtig beantwortet! Glückwunsch, " + name)
    elif punkte >= 0.7 * anzahl_fragen:
        print("ja, gingso")
    elif punkte >= 0.5 * anzahl_fragen:
        print("Ist ja gerade noch gut gegangen")
    else:
        print("naja")
~~~


## Dictionaries!
~~~
keller = {"fahrrad":8,       "lastenrad":3,      "roller":7,      "rollschuhe":3,      "skateboards":3}
~~~

**Ein dictionary besteht aus key-value-Paaren, die durch einen Doppelpunkt getrennt werden.**
Zugriff auf die Werte:
~~~
for ding in keller:
    print(ding)
    print(keller[ding])
~~~
Vergleich zu Listen:
~~~
farbenliste = ["rot", "gruen", "gelb", "orange"]
print(farbenliste[2])
farbendictionary = {0:"rot", 1:"gruen", 2:"gelb", 3:"orange"}
print(farbendictionary[2])
~~~

Keys und Values:
~~~
print(keller.keys())
print(keller.values())
~~~

Neue Dinge im Keller:

Die Values verändern:
~~~
keller["fahrrad"] = 3  # Die Anzahl der Fahrräder direkt auf 3 setzen
keller["fahrrad"] += 1  # Die Anzahl der Fahrräder um eins erhöhen
~~~
Die Keys verändern:
~~~
keller["monster"] = 1  # eine neue Objektesorte
keller["Fahrrad"] = 4  # Aufpassen bei Groß- und Kleinschreibung

print(keller)
~~~


## Aufgabe 2 – nochmal genauer.
Aufgabenstellung:

Vervollständigen, korrigieren;  
Wie einbauen?
~~~
def fragestellen(frage, richtige_antwort):
    """stelle eine frage und überprüfe ob die gegebene Antwort richtige_antwort ist."""
    # Frage stellen und Antwort speichern
    gegebene_antwort = input(frage + "    ")  # Leerzeichen für schöneres printen.
    # Überprüfen, ob die Antwort richtig ist
    if gegebene_antwort == richtige_antwort:
        # Wenn ja: Ein Lob printen und True zurückgeben
        print("Gut gemacht.")
        return True
    # Wenn nein: False zurückgeben.
    else:
        print("Nicht gut!")
        return False
~~~
Einbauen: Funktion gibt True/False aus => passt gut in eine if-Verzweigung.
