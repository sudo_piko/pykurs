# Pikos Python Kurs 28.09.2021

## Organisatorisches
### Gruppenliste
https://md.darmstadt.ccc.de/uUxnBOlqS8eKN09CfTd3hQ?edit
### Mailingliste
https://lists.haecksen.org/listinfo/python

Funktioniert jetzt, bitte subscribt euch, kann sein dass Piko auch mal ne Absage schicken muss.

## Hausaufgaben vom 21.09.2021
Ging bei den meisten alleine oder in der Gruppe
### Aufgabe 1
### Was tut das hier?
```
from turtle import *
color('red', 'yellow')
begin_fill()
for i in range(36):
    forward(200)
    left(170)
end_fill()
done()
```
Ändert den Code ab und malt andere Mandalas!
Was müsst Ihr tun, um gebogene Striche zu bekommen?

Antwort: einen 36-zackigen Stern! Für gebogene Formen hilft die Funktion circle(). Wer ganz viel Zeit hat kann auch einen Kreis/Bögen aus ganz vielen kleinen Ecken erzeugen.

Spezialfragen zu done(), Endlosschleifen, und fill-Funktion wurden behandelt.

Mandalas:
https://md.ha.si/fcFBdrTxQpigHksfe0JA6g?both#
(Gerne weiter ergänzen!)

### Aufgabe 2 Verzweigungen 
**Wurde im Kurs nicht besprochen weil alle sich recht sicher waren, hier daher (vorerst) nur waldwesens Kommentare und Lösungen.**
### Gleich
Vorsicht: im Folgenden sind – wie bei den For-Schleifen – die Einrückungen sehr wichtig!
```
a = 5
if a == 5:
    print("Bitte nicht dran ziehen!") #hier ist a immer 5, die if-Aussage ist wahr.

a = a + 1
if a == 5:
    print("Hält mit Gaffa.") #hier läuft er nicht rein, weil a dann schon größer geworden ist
else:
    print("") #das erzeugt eine Leerzeile
```
wenn man die == in != ("ungleich") ändert, "Kommt Hält mit Gaffa" raus, weil damit der Sinn umgekehrt wird

### Ungleich
```
b = 4
if b < 3:
    print("Boah, b ist ja nicht mal eine natürliche Zahl!")
elif b < 4:
    print("b ist mir nicht groß genug.")
elif b < 6:
    print("b ist eine gute, angemessen große Zahl.")
else:
    print("Boah, bitte geh weg mit diesen riesen Zahlen.") #hier passiert immer was, als letzter backup dieser Satz

alter = 16

if 12 <= alter < 18: #
    print("Neuerdings auf impfbar!")
```
Wenn man dasselbe mit strings macht, wird wieder anhand der ASCII Tabelle verglichen

### AND, OR
```
a = 100
b = 22
c = 300
if a > b and c > a:
    print("Both conditions are True")
```
OR statt AND führt dazu, dass nur eine der beiden Prüfungen wahr sein muss. Per default passiert hier im Ergebnis nichts anderes, weil ja von Anfang an beides wahr ist.

### Experimente mit der Kombo von AND und OR
```
a = 100
b = 22
c = 300
if a == b and b < a or c == a:
    print("Both conditions are Trueee")
```
dieses if ist nicht wahr!
```
a = 100
b = 22
c = 300
if b < a or a == b and c == a:
    print("Both conditions are True2")
```
Es wird erst das "and", dann das "or" abgeprüft, denn das hier ist wahr! Im echten Leben sollte man hier wohl dringend Klammern setzen, um die Logik zu bekommen die man haben möchte...

## Themen aus der Stunde 

### Wahrheit
a=5
wenn man dann
a == 5
macht, dann erscheint True! True ist ein boolescher (siehe [Wikipedia](https://de.wikipedia.org/wiki/Boolescher_Operator)) Wert (Objekttyp: bool). False ist der Gegenwert (eine Kondition wird nicht erfüllt).
True und False werden in Python immer großgeschrieben, weil sie so wichtig sind. Es sind in Python quasi reservierte Wörter (wenn großgeschrieben).

Hinter einem if oder einem elif erscheinen immer Dinge, die zu einem boolschen Wert führen.
Der int(True) ist übrigens 1, der int(False) ist 0 ;)

In einem if-statement wird alles, was nicht 0 ist zu True ausgewertet (kann zu komischen Fehlern führen).
Beispiel: 
```
if 42:
	print("Sinn des Lebens!")
```
...funktioniert :)

### OR und AND
Geht heute die Welt unter? - nur wenn Piko in München gewesen wäre **und** die Sonne geschienen hätte!
Zum Glück war Piko nicht in München, puh.

### While-Schleife
While heißt "solange", d.h.
```
a = 6
while a < 10:
    print(a)
    a = a + 1
```
...wird 4 mal durchlaufen, bis a = 10 ist. Dann wird der Schleifenkopf nicht noch einmal ausgeführt.
Dauerschleife: While True : - da muss dann das innere der Schleife dafür sorgen, dass sie irgendwann endet!
Keyboardinterrupt: meistens STRG+C

### Zahlen
Der Typ Float ist eine Art, Dezimalzahlen darzustellen, wird immer mit Punkt geschrieben (2.5 ist eine Zahl, 2,5 nicht!)
Dazu gibts eine Hausaufgabe, es gibt auch noch komplexe Zahlen. Wem das zu viel ist, der kann die auch weglassen.
Binärsystem: 0b11 ist z.B. 3 (b für binär)
Oktal: mit o (alles weitere im Hausaufgabentext, in gemütlich)

### Frage: gibt es eine Möglichkeit die Anzahl der durchlaufenen Schleifen anzugeben?
Da kann man eigentlich nur eine weitere Variable hochzählen, in Pikos Beispiel ist das i, was dann einfach per print ausgegeben wird.

### input()
Kommt in der Hausaufgabe

### If und ihre Begleiter*innen
Nach einem If kann man Else oder Elif nutzen um verschiedene, unterschiedlich bevorzugte Alternativen auszudrücken (Wenn du: ein Klavier hast, spiel Klavier. Sonst Geige. [...] Wenn du nichts hast, klatsch in die Hände.)
Das sollen wir uns ggfs. selbst nochmal in Ruhe angucken, das ist wichtig.

### IDE benutzen
Ab jetzt möglichst alle PyCharm benutzen, es hat viele Funktionen und es ist gut.
Ganz unten gibt es auch weiterhin eine Konsole, man kann sich aber auch Dateien angucken und abspeichern.

Download Link: https://www.jetbrains.com/pycharm/download/

Falls jmd. sich damit nicht wohlfühlt oder erst später damit anfangen möchte ist das auch ok, aber es ist eine wärmste Empfehlung :)
Andere IDEs sind auch ok, da kann Piko nur ggfs. nicht gut helfen.

### Termine
Gibts in der Haecksenwolke (Link für Menschen auf der Mailingliste auf Anfrage)

## Pikos Idle
```
a = 5
if a == 5:
	print("a ist wirklich fünf")
a == 5
1 + 2
ord("a")
a == 5
type(True)
not True
# boolean
if True:
	print("a ist...")
a = a + 1
a
if a == 5:
	print("uiae")
a == 5
if False:
	print("uiae")
A = 7
a
A
int(True)
int(False)
if 0:
	print("lalal")
if 1:
	print("Uiuiuiui")
if 42:
	print("Sinn des Lebens!")
if 0.000000001:
	print("uaei")
if "hui":
	print("hui")
True and False
sonnenschein_in
sonnenschein_in_hamburg = True
piko_in_muenchen = False
if sonnenschein_in_hamburg and piko_in_muenchen:
	weltuntergang = True
else:
	weltuntergang = False
if weltuntergang:
	print("AAAAAAAAAaaaaaaaaaaaaaaaaAAAAAAAAAAAaaaaaaaaa")
sonnenschein_in_hamburg and piko_in_muenchen
while a < 10:
	print(a)
```
Hier wurden ganz viele "6"en ausgegeben...
```
...
6
6
6
6
6
6
^CTraceback (most recent call last):
  File "<stdin>", line 2, in <module>
KeyboardInterrupt
6
>>> while a < 10:
...     print(a)
...     a = a + 1
... 
6
7
8
9
>>> a
10
>>> a < 10
False
>>> while True:
KeyboardInterrupt
>>> type(2.5)
<class 'float'>
>>> int(2.5)
2
>>> float(2)
2.0
>>> i = 0
>>> a = 20
>>> while a > 100:
...     print("huiuiui", a)
...     a = a + 5
...     i = i + 1
...     print("       (Wir sind in der " + str(i) + "-ten Schleife.)")
... 
>>> while a < 100:
...     print("huiuiui", a)
...     a = a + 5
...     i = i + 1
...     print("       (Wir sind in der " + str(i) + "-ten Schleife.)")
... 
huiuiui 20
       (Wir sind in der 1-ten Schleife.)
huiuiui 25
       (Wir sind in der 2-ten Schleife.)
huiuiui 30
       (Wir sind in der 3-ten Schleife.)
huiuiui 35
       (Wir sind in der 4-ten Schleife.)
huiuiui 40
       (Wir sind in der 5-ten Schleife.)
huiuiui 45
       (Wir sind in der 6-ten Schleife.)
huiuiui 50
       (Wir sind in der 7-ten Schleife.)
huiuiui 55
       (Wir sind in der 8-ten Schleife.)
huiuiui 60
       (Wir sind in der 9-ten Schleife.)
huiuiui 65
       (Wir sind in der 10-ten Schleife.)
huiuiui 70
       (Wir sind in der 11-ten Schleife.)
huiuiui 75
       (Wir sind in der 12-ten Schleife.)
huiuiui 80
       (Wir sind in der 13-ten Schleife.)
huiuiui 85
       (Wir sind in der 14-ten Schleife.)
huiuiui 90
       (Wir sind in der 15-ten Schleife.)
huiuiui 95
       (Wir sind in der 16-ten Schleife.)
>>> einezahl = input("Gib mir eine Zahl! ")
Gib mir eine Zahl! 4
>>> einezahl
'4'
```
