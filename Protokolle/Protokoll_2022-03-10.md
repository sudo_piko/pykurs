# Pikos Python Kurs am 10.03.2022


[TOC]


## Mitschrift 
### Orga und Sonstiges 

Aufruf an Team Python. Leute auf mastodon können abstimmen: Welche ist die beste Programmiersprache der Welt? 
https://chaos.social/web/statuses/107932999347772214

Wer mag, kann hier seine Hausaufgaben/Lösungen reinpacken für eine Code-Review durch Piko
https://md.ha.si/ZD3m5BgrRJCKs_zezqAQQQ#

Status-Update cardio:
Bedarf ist ermittelt, die ersten sind auch schon unterwegs. Jemand will und hat noch nicht? bitte melden.

### Cheat-Sheet: Die Bedeutung von Punkten in Python
erklärt auch die Gemeinsamkeiten und Unterschiede von Methoden und Funktionen
(Details siehe Cheat-Sheet, hab nicht alles mitgeschrieben)

Punkte kommen beispielsweise vor....
... wenn man etwas importierten muss (ein Modul). Dann steht hinter dem Punkt eine Funktion (und ein oder mehrere argumente in der Klammer). Beispiel: turtle.circle(10,120)

... wenn man eine Methode auf ein Objekt anwendet, das man vorher definiert hat (class) oder das von Python vordefiniert ist (zum Beispiel Listen-Objekte)

... wen man auf Attribute von Objekten zugreifen will - das geht nach dem Muster objekt.attribut
	Zum Beispiel: fahrrad1.farbe (--> keine Klammer!) gibt die Farbe aus. 

### Funktionen und Methoden - Gemeinsamkeiten und Unterschiede
- beide haben Klammern
- "eigentliche" Funktionen können alleine stehen (z.B. print())
- andere hängen an einem Modul z.B. turtle.fd(10)
- Methoden können manchmal das dazu gehörende Objekt verändern (z.B: liste.append())
	anderes Beispiel: 
	list = [6, 2, 4, 5]
	list.sort() -> ändert die Reihenfolge (Listen-Items bekommen einen neuen Index!)

Übersichtstabelle: 
https://md.darmstadt.ccc.de/uploads/upload_af214c16f159db8da717a65e1a63c421.png

### Hausaufgaben Besprechung

Frage: Kann ich die Eigenschaften eines Objekts durch die Methoden ändern und wie prüfe ich das?
Konkret: Macht das Ausführen von fahrrad1.kaputtmachen() das Fahrrad wirklich kaputt, sprich: wird aus False dann True? 

### Ausführung siehe Code#

Um Attribute an einem Objekt zu prüfen, rufen wie es so auf:  
print(fahrrad1.kaputt) -> Ausgabe: Fahrrad ist noch heil (False)  
fahrrad1.kaputtmachen() -> Fahrrad wird kaputt gemacht  
print(fahrrad1.kaputt) -> neue Ausgabe: Fahrrad ist jetzt kaputt (True)  

Um das ganze verständlicher zu machen, definieren wir eine neue Funktion
```python
def kaputtheitssatus_printen(self):  
print(f"Das Fahrrad von {self.besitzy} ist kaputt: {self.kaputt}")
fahrrad1.kaputtheitsstatus_printen()

```

noch eleganter: Eine Funktion, die nur den Status ausgibt und deren Ausgabe dann weiter verwendet werden kann. 

```
def ist_kaputt():
return self.kaputt 
```

mit der durch "return" abgespeicherten Ausgabe (True oder False) können wir jetzt weiterarbeiten und typische True/False-Dinge machen. Zum Beispiel eine if-Verknüpfung

Anmerkung: if fahrrad1.ist_kaputt() braucht kein == True/False danach, weil die Funktion ja so definiert ist, dass sie nur True/False annehmen kann. 
anders z.B. 

```
if x == %2:  
		... (ermittelt, ob ein Zahlenwert durch zwei teilbar ist)

```


### Neue Hausaufgabe: wie definiere ein neues Objekt, ein Dreieck
welche Attribute brauchen wir?  Wir haben uns auf laenge, farbe, fuellfarbe, strichdicke, winkelrundung verständigt. 
nächster Schritt: Methoden erstellen für malen() und um Werte ausgeben zu können

Sidenote: 
t = turtle.Turtle() -> Turtle ist auch ein Objekt. Die Klasse "turtle" wird aufgerufen und Turtle ist das Objekt

wenn man zu Beginn from turtle import * macht, muss man turtle nicht bei jeder Funktion aufrufen. (wir erinnern und an die ausgeschüttete Lego-Kiste vs. das aufgeräumte Zimmer, in dem man immer sagen muss, in welcher Kiste etwas liegt) 

Frage: Warum braucht es, wenn wir eine neue class definieren, zu Beginn diese Auflistung
```python
self.laenge =  laenge
self.farbe = farbe
self.fuellfarbe = fuellfarbe 
usw. 

```

Es wirkt repetitiv und umständlich, ist aber notwendig. Auf diese Art wird das zuerst "leere" Objekt mit Inhalt "befüllt". 
--> Nur so können Informationen darin gespeichert werden. 








 

## Pikos Datei
```python

class Fahrrad:

    # __init__ baut uns ein neues Fahrrad-Objekt:
    # def __init__(self, besitzy, farbe="blau", reifen_groesse=28, kaputt=False):
    def __init__(self, besitzy, farbe, reifen_groesse, kaputt):
        self.farbe = farbe
        self.reifen_groesse = reifen_groesse
        self.kaputt = kaputt
        self.besitzy = besitzy

    def kaputtmachen(self):
        self.kaputt = True

    def reparieren(self):
        self.kaputt = False

    def kaputtheitsstatus_printen(self):
        print(f"Das Fahrrad von {self.besitzy} ist kaputt: {self.kaputt}")

    def ist_kaputt(self):
        return self.kaputt

    def verschenken_an(self, beschenkte_person):
        self.besitzy = beschenkte_person

    def wert_zeigen(self):
        print(f"Das Fahrrad von {self.besitzy} ist {self.farbe} und hat die Reifengröße {self.reifen_groesse}")

    def malen(self):
        t = turtle.Turtle()
        color = False
        # Hinterreifen
        t.pd()
        t.circle(self.reifen_groesse)
        t.pu()
        # Sattel und Stange
        t.fd(self.reifen_groesse / 2)
        t.pd()
        t.lt(90)
        t.fd()



# Das lässt sich dann so aufrufen:
fahrrad1 = Fahrrad("Ada", "blau", 24, False)
# fahrrad2 = Fahrrad("Margaret", "weiß")
# fahrrad1.wert_zeigen()


fahrrad1.kaputtheitsstatus_printen()
fahrrad1.kaputtmachen()
fahrrad1.kaputtheitsstatus_printen()

# Kaputtheitsstatus verwenden:
if fahrrad1.kaputt:
    pass
# äquivalent zur oben definierten Methode
if fahrrad1.ist_kaputt():
    print("Ohnein! Adas Fahrrad ist kaputt!")


## if will eigentlich nur True oder False, die Ausdrücke nach "if" werden ausgewertet, bevor if sie verwenden kann
# if xxx == yyy:
#
# if x != y:
# if x > y:
## Wird zu:
# if True
## So ähnlich wie folgendes, da wird auch erstmal 3+5 gerechnet und dann mit der 8 weitergemacht
# print(3 + 5)

```
Hausaufgabe 2

```python

# Definiert eine Klasse namens Dreieck. Welche Attribute könnte denn ein Dreieck so haben?
# Gebt ihr die beiden Methoden "malen" und "Werte ausgeben"

liste = [6, 2, 3]
liste.append(5)
liste.index(3)
liste.sort()

import turtle

class Dreieck:

    def __init__(self, laenge, farbe, fuellfarbe, strichdicke, winkelrundung):
        self.laenge = laenge
        self.farbe = farbe
        self.fuellfarbe = fuellfarbe
        self.strichdicke = strichdicke
        self.winkelrundung = winkelrundung

    def malen(self):
        t = turtle.Turtle()
        #stiftfarbe etc einstellen
        for i in range(3):
            t.fd(self.laenge - 2 * self.winkelrundung)
            # t.lt(120)  # mit spitzen Ecken
            t.circle(self.winkelrundung, extent=120)




adas_dreieck = Dreieck(400, "red", "blue", 3, 50)

adas_dreieck.besitzy = "Ada"
adas_dreieck.lebenswunsch = "Paris besuchen"
adas_dreieck.anzahl_katzen = 20

adas_dreieck.malen()


turtle.exitonclick()



```