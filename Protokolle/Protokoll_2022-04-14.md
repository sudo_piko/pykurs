# Pikos Python Kurs am 14.04.2022

Nächste Woche: Einführung in Git ohne Piko

Wir haben uns die Firmware der Card10 genauer angesehen, besonders main.py

Außerdem haben wir über imports gesprochen, siehe im Ordner Kleines/Obstsalat

## Protokoll
Python Kurs mit Piko vom 14. April 2022

Nächste Woche: Einführung in Git

https://doku.haecksen.org/books/python-kurs
 —> Wikiartikel

Ein Artikel in Datenschleuder wird erscheinen.

Python Kurs für Jugend hackt von Piko, den können wir auch durcharbeiten. Andere Herangehensweise als bei uns im Kurs. Am Ende wird Pong programmiert. https://jugendhackt.org/oer/projekte/python-einfuehrung

——————————

Frage nach Sensoren vorstellen: leider können heute keine Sensoren vorgestellt werden.

Heute: Auf der Card10 website umgucken https://card10.badge.events.ccc.de/
Die Seite ist “der zentrale newshub” für die Card10
News
Updates der firmware

Firmware (https://card10.badge.events.ccc.de/firmware/firmwareupdate/)
—> Queer Quinoa (aktuelle firmware Version) runterladen
Was sind .zip Files? —> komprimierte Daten, es gibt noch andere Algorithmen aber .zip ist der bekannteste

Next step: entpacken (meistens durch Doppelklick)

Wir schauen in die Ordner, welche Dateien sind da drin, was ist in den Dateien (10min).

main.py

Wir besprechen die Import Funktion in Python (das ist zu komplex für ein fortlaufendes Protokoll xD) . Wenn z.B. auf der Card10 ein Ordner ist, in dem eine Python Datei ist, in der z.B. Funktionen definiert sind, dann kann man durch das Importieren auch in der Ausgangsdatei durch Import die Funktionen aus der importierten Datei zugreifen.

Bsp.:
1. Datei “banana.py”
 def schaelen(anzahl):
    print(“Diese Frucht ist geschält”*anzahl)

2. Datei: “obst.py”
Import banana
—-> die schaelen Funktion kann nun auch in obst.py benutzt werden

Müssen im selben Ordner sein oder im Unterordner.

Die Code Zeile “if __name__= “__main__:” und das folgende bedeutet, dass wenn die Datei als Hauptprogramm ausgeführt wird, soll das geschehen, wenn es nicht das Hauptprogramm ist, dann nicht. (Also wenn banana.py ausgeführt wird, dann wird auch die “if __name__ […]” Zeile ausgeführt, wenn aber obst.py ausgeführt wird, dann wird der Teil nicht ausgeführt)

Die Unterstriche weisen daraufhin, dass Python etwas im Hintergrund macht.
Was hat das mit der Card10 zu tun? Eins hat eine Uhr programmiert. Wenn man die ausführt, dann wird es als Hauptprogramm ausgeführt. Ein anderes möchte eine andere Uhr programmieren und auf Funktionen aus dem bereits programmierten zugreifen aber eben nicht zwei Uhren gleichzeitig ausführen.

Wir haben mit Obstsalat gespielt um das gelernte zu visualisieren, der Code ist auf pikos Gitlab.






## Links

- https://card10.badge.events.ccc.de/firmware/firmwareupdate/
- https://doku.haecksen.org/books/python-kurs
- https://jugendhackt.org/oer/projekte/python-einfuehrung/
