# Pikos Python Kurs am 03.03.2022


[TOC]


## Mitschrift 
Protokoll: Balub

### Organisatorisches

#### Links:

* Gitlab (Aufgaben und Protokolle)
  https://gitlab.com/sudo_piko/pykurs
* Mailingliste
  https://lists.haecksen.org/listinfo/python
* Kontakt zu Piko: Python@ithea.de
* Gruppen-Pad: https://md.darmstadt.ccc.de/uUxnBOlqS8eKN09CfTd3hQ?view# gerne zur Selbstorganisation nutzen! Ist aber möglicherweise nicht aktuell. --> *Lieber im Chat oder auf der Mailingliste fragen*

#### Card10-Bedarf:

- 10 werden noch benötigt (**Bedarf: 9** (1 sucht selbst))

### Python

#### Pads:

- Codeschnipsel während der Stunde: https://md.ha.si/ZD3m5BgrRJCKs_zezqAQQQ

#### Mastermind

Heute nicht.

#### Klassen und Objekte und Methoden

- **Klassen** sind Baupläne/Backrezepte für Objekte. Man sagt: **Klassen sind Objekttypen**. *Also sowas wie Integer, Fahrrad, ...*

- **Objekte** haben Eigenschaften und Methoden. **Methoden sind Funktionen, die man nur auf bestimmte Objekte anwenden kann**. *Fahrrad1 ist ein Objekt der Klasse Fahrrad und darauf kann man die Methode malen() anwenden.*

  - Aus der Hausaufgabe:

    - init(self) ist eine Funktion, mit der man Objekte der Klasse bauen kann. Die definiert die Objekte. Also Fahrradklasse ist ein Bauplan für ein Fahrrad. Aber um ein konkretes Fahrrad zu bauen, muss man die **init-Funktion** verwenden.

      Die sieht komisch aus und kann Dinge bauen, ist aber eigentlich eine Funktion wie alle anderen auch. 

      ```python
      def __init__(self, besitzy, farbe="blau", reifen_groesse=28, kaputt=False):
          ...
      ```

    - Eigenschaften sind das hier. Die nennt man **Attribute**

      ```python
      def __init__(self, besitzy, farbe="blau", reifen_groesse=28, kaputt=False):
          self.farbe = farbe
          self.reifen_groesse = reifen_groesse
          self.kaputt = kaputt
          self.besitzy = besitzy
      ```

      Darauf kannste über Methoden oder auch direkt zugreifen. Zum Beispiel so:

      ```python
      fahrrad1 = Fahrrad("Ada", "blue")
      print(fahrrad1.besitzy) #Ada
      ```

      

    - **Methoden** sind hier sowas wie malen(), kaputtmachen(), wert_anzeigen(),..:

      ```python
      def wert_zeigen(self):
          print(f"Das Fahrrad von {self.besitzy} ist {self.farbe} und hat die Reifengröße {self.reifen_groesse}")
          
      #aufrufen geht dann so und setzt den Besitzer, die Farbe und die Reifengröße von Fahrrad3 ein. (Fahrrad3 vorne ersetzt das self hinten beim Aufruf)
      fahrrad3.wert_anzeigen()
        
      ```

#### Pikos-Kisten-Metapher&trade;

```python
from turtle import * 
# nimmt die Legokiste (turtle) und leert sie aus. Die Teile kann man dann einfach verwenden. So wie pendown(), aber das Zimmer ist unordentlich.
```

```python
import turtle
# stellt nur die Kiste irgendwo hin und wenn man da Dinge draus braucht, muss man immer angeben, dass die aus der Kiste kommen. Also mit der Punktschreibweise: turtle.exitonclick(). Das Zimmer bleibt aber ordentlich.
```




#### Sonstiges Spannendes am Rande zum selber vertiefen

- Python ist eine **objektorientierte Programmiersprache** (nicht nur und nicht ganz, aber das ist das was es für uns spannend macht)
  - Wikipedia sagt das dazu: "Python unterstützt mehrere [Programmierparadigmen](https://de.wikipedia.org/wiki/Programmierparadigma), z. B. die [objektorientierte](https://de.wikipedia.org/wiki/Objektorientierte_Programmierung), die [aspektorientierte](https://de.wikipedia.org/wiki/Aspektorientierte_Programmierung) und die [funktionale](https://de.wikipedia.org/wiki/Funktionale_Programmierung) Programmierung"

#### BBB-Foo

Wird keine Umfrage angezeigt? --> Einmal kurz F5 (Vollbild) und zurück kann Abhilfe schaffen



## Notizen im BBB
Klasse mit Großbuchstaben ist ein Objekttyp, wie Listen  und Integers. Objekte haben Methoden. Das sind Funktionen, die man nur auf bestimmte Objekte anwenden kann. z.b. append.list append ist eine Funktion, die nur mit dem Typ liste verwendet werden kann, deshalb ist sie eine Methode, eine Spezialfunktion für spezielle Typen. 

Ich kann diese Methoden selbst definieren und mit (self) füllen. im self kann man default Werte angeben.


## Desiderats Datei

```python

import turtle 
#from turtle import *
import tkinter as tk
####Code angepasst und erweitert:


class Fahrrad:
    
    # __init__ baut uns ein neues Fahrrad-Objekt:
    def __init__(self, besitzy,farbe, reifen_groesse, kaputt=False):
        self.farbe = farbe #Attribut
        self.reifen_groesse = reifen_groesse
        self.kaputt = kaputt
        self.besitzy = besitzy
    
    def kaputtmachen(self):
        self.kaputt = True
        
    def reparieren(self):
        self.kaputt = False
        
    def verschenken_an(self, beschenkte_person):
        self.besitzy = beschenkte_person
    
    def wert_zeigen(self):
        print(f"Das Fahrrad von {self.besitzy} ist {self.farbe} und hat die Reifengröße {self.reifen_groesse}")

    def malen(self):
        t = turtle.Turtle()
        t.pensize(2)
        t.pencolor(self.farbe)
        t.lt(120)
        # Hinterreifen
        t.pd()
        t.circle(self.reifen_groesse)
        t.pu()
        # Sattel und Stange
        t.lt(75)
        t.fd(10)
        t.lt(25)
        t.fd(self.reifen_groesse/2)
        t.pd()
        t.rt(45)
        t.fd(100)       
        # Vorderreifen
        t.pd()
        t.circle(self.reifen_groesse)
      
        #Stange und Lenker
        t.rt(75)
        t.fd(40)
        t.rt(70)
        t.fd(25)

# Das lässt sich dann so aufrufen:
fahrrad1 = Fahrrad("Ada", "blue",24)
fahrrad2 = Fahrrad("Margaret", "red",12)
#Statt Klassen kann man mit Dictionaries arbeiten, aber es ist umständlich:
fahrrad3={"besitzy":"Ada","farbe":"green","reifen_groesse":28}

#fahrrad1.wert_zeigen()
fahrrad1.malen()
#turtle.fd(200)
#fahrrad2.malen()


#tk.mainloop()
turtle.exitonclick()  
```

## Pikos Datei
```python
import turtle

def dreieck(laenge, strichdicke=3):
    turtle.pensize(strichdicke)
    for i in range(3):
        turtle.fd(laenge)
        turtle.rt(120)

dreieck(100)

class Fahrrad:

    # __init__ baut uns ein neues Fahrrad-Objekt:
    # def __init__(self, besitzy, farbe="blau", reifen_groesse=28, kaputt=False):
    def __init__(self, besitzy, farbe, reifen_groesse, kaputt):
        self.farbe = farbe
        self.reifen_groesse = reifen_groesse
        self.kaputt = kaputt
        self.besitzy = besitzy

    def kaputtmachen(self):
        self.kaputt = True

    def reparieren(self):
        self.kaputt = False

    def verschenken_an(self, beschenkte_person):
        self.besitzy = beschenkte_person

    def wert_zeigen(self):
        print(f"Das Fahrrad von {self.besitzy} ist {self.farbe} und hat die Reifengröße {self.reifen_groesse}")

    def malen(self):
        t = turtle.Turtle()
        color = False
        # Hinterreifen
        t.pd()
        t.circle(self.reifen_groesse)
        t.pu()
        # Sattel und Stange
        t.fd(self.reifen_groesse / 2)
        t.pd()
        t.lt(90)
        t.fd()



# Das lässt sich dann so aufrufen:
fahrrad1 = Fahrrad("Ada", "blau", 24, False)
# fahrrad2 = Fahrrad("Margaret", "weiß")
# fahrrad1.wert_zeigen()
print(fahrrad1.besitzy)

zahl = 5
print(zahl)

liste = [1,2]
liste.append(3)
```