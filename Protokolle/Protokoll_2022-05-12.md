# Pikos Python Kurs am 12.05.2022


- north_sense verändern
- andere Apps
- range nachbauen
- Doku anschauen

## Links
Die Doku für die Python-Module auf der Card10:  
https://firmware.card10.badge.events.ccc.de/index.html

App-Empfehlungen:  
https://md.ha.si/ACSqiK7UQeubRf1eDu_0ng#

Firmware-Updaten:  
https://card10.badge.events.ccc.de/firmware/firmwareupdate/


## Pikos Datei
```python
for i in range(5, 20, 2):
    print("Zweimal", i, "ist:", i*2)

# range         default  non-default  default  # built-in
# rangenachbau   non-d.    non-d.    default
def rangenachbau(minimum,  maximum,  schritt=1):
    ausgabe = []
    while minimum < maximum:
        ausgabe.append(minimum)
        minimum += schritt
    return ausgabe

print(rangenachbau(3, 10))
print(type(range(3, 10)))
```
Range ist "fauler" als unser Nachbau, es rechnet die nächste Zahl erst aus, wenn es danach gefragt wird.
Das Wort, das Ihr mit diesem Problem verbinden sollt, ist "yield"