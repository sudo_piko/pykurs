# Pikos Python Kurs am 27.01.2022

## Mitschrift 

"Protokoll" -> wild Zeugs zusammengetrümmert.

## Dinge zu Hausaufgabe 1:

### Dialogfenster in Python:

Beispielcode aus einer Lösung zu Hausaufgabe 1:

```versuche = numinput(“Anzahl Versuche”, “Wieviele Versuche möchtest du?”, 6, 3, 10)

codelaenge = numinput(“Anzahl Codepins”, “Wieviel Stellen soll der Code haben?”, 4, 4, 10)
```
### Links zu den HedgeDocs

Code der anderen Lösungen: https://md.ha.si/cMGZMFhDSNqmVxsTNvnBww#

Was brauchen wir für Mastermind: https://md.ha.si/J4Bb4z7uQ9e15_MWLprDZw?both

### Pikos Code

Achtung, kann sein, dass ich nicht alles mitbekommen habe.

```import turtle

RADIUS = 20

fenster = turtle.Screen{}
print(type(fenster))
fenster.setup(width=400, height=800) #Das hier noch an die Slots anpassen

schildi1 = turtle.Turtle()

schildi1.speed(0)
schildi1.hideturtle()
schildi1.pu()
schildi1.goto(-100, 200)

def farbkreis(farbe):
    schildi1.pd()
    schildi1.color("black", farbe)
    schildi1.begin_fill()
    schildi1.circle(RADIUS)
    schildi1.end_fill()
    schildi1.pu()

farbkreis()

def blablabla(farben):
    for farbe in farben:
        farbkreis(farbe)
        schildi1.fd(ENTFERNUNG)

fenster.exitonclick()
```

### Import-Varianten

```from turtle import *
```
* Greift nut auf die Methoden als Funktionen zu

```import turtle
```

* Damit können besser Objekte erstellt/benutzt werden
* Wichtig für das Anwenden spezifischer Funktionen auf nur 1 Objekt
* Beispiel: schildi1 und schildi2 -> können etwa unterschiedliche farben haben
  * schildi1.color("blue")
  * schildi2.color("green")
* sorgt auch für guten Überblick und bewussten Einsatz der Funktionen

### Mastermind Voraussetzung

(bezieht sich auf das HedgeDoc)

Finja: "wie man das in Zeile 22 loest: erstmal fuer jede Farbe im eigegebenem Versuch gucken, ob die FArbe im Code ist -> wenn ja, weiss hochzaehlen. Dann schauen, ob auch die Position gleich ist -> wenn ja, weiss wieder runetrzaehlen, und rot oder schwarz hochzaehlen"

Pseudocode (echter richtiger benutzbarer Begriff ^^ ) von clx:

```if color anwesend
gib weiss
if color in right place
take weiss
gib rot
else
gib nix
```

## Basics für unser Mastermind

(Inhalt kopiert aus HedgeDoc: https://md.ha.si/J4Bb4z7uQ9e15_MWLprDZw?both)

# Muss sein

## Codea-Hygiene
definition: ws ist mastermind, was passiert da??? (DER docstring): rate einen mehrteiligen Farbcode. bekomme hinweise auf richtige Farben/Positionen nach jedem Raten)

## Programm-Logik
Konstanten:
* Anzahl Versuche festlegen und runterzählen
* Anzahl Farben (insgesamt und im Farb-Code)
* Anzahl Slots

random importieren für zufälligen Farb-Code: Irgendwas, das Farb-Code am Anfang festlegt


input mit Lösungsversuch

Lösungsversuch und Lösung (= Farb-Code) vergleichen

Punkte für richtige Farbe an richtiger Stelle (im Spiel rot oder schwarz) und Punkte für richtige Farbe, aber an falscher Stelle (im Spiel weiß).


error für unvollständigen guess (= nicht genug Farben im Lösungsversuch angegeben)



vorzeitiges Beenden, wenn der Code vor Ende der Versuche schon richtig erraten ist


## Graphik
die richtigen Farben: perfekte Farbnamen (oder Hex-Code für Farben)
Anzahl Farben insgesamt: z.B. RED, GREEN, YELLOW, BLUE, BLACK, ORANGE





# Nice to haves


ausgabe punkte am ende

Einstellen können, ob es Duplikate geben darf
lass user entscheiden wie viele farben, versuche, doubles or not
Lösungsversuch zum Klicken

statt nur die schlichte Ausgabe der Punkte am Ende: zusätzlich irgendwas Lustiges (z.B. ein turtle-Bild malen das bunter ist je mehr Punkte erreicht wurden)
Anzeiger für richtig und falsch haben Haecksehut-Form!



# Pad

https://md.ha.si/J4Bb4z7uQ9e15_MWLprDZw?edit  
https://md.ha.si/cMGZMFhDSNqmVxsTNvnBww#

## Pikos Datei

```python

import turtle

RADIUS = 20

# Turtle setup
fenster = turtle.Screen()
print(type(fenster))
fenster.setup(width=400, height=800)  # Das hier noch an die Slots anpassen

schildi1 = turtle.Turtle()

schildi1.speed(0)
schildi1.hideturtle()
schildi1.pu()
schildi1.goto(-100, 280)
#
# schildi2 = turtle.Turtle()
# schildi2.shape("turtle")
# schildi2.shapesize(20)

# Graphik-Funktionen
def farbkreis(farbe):
    schildi1.pd()
    schildi1.color("black", farbe)
    schildi1.begin_fill()
    schildi1.circle(RADIUS)
    schildi1.end_fill()
    schildi1.pu()

farbkreis("red")

def blablabla(farben):
    for farbe in farben:
        farbkreis(farbe)
        schildi1.fd(ENTFERNUNG)
    ...

fenster.exitonclick()
```