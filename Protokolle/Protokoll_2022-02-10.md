# Pikos Python Kurs am 10.02.2022

## Mitschrift 

# Pads
Von den letzten Malen:  
https://md.ha.si/cMGZMFhDSNqmVxsTNvnBww#  
https://md.ha.si/VTjPab4PSQyUvat8TnLXBw#

Von diesem Mal:  
https://md.ha.si/uSTHY1VhS12M60UKxi8rpA?edit

## Pikos Datei
Siehe auch Mastermindprobieren.py


Was alles von if als "False" gelesen werden kann:
```python
x = False # oder: 0, None, ""
x = 0.000000001

if x:
    print("ja!")
```

enumerate ist praktisch: 
```python
liste = ["a", "b", "c","d"]
neu = enumerate(liste)  # => [(0, "a"), (1, "b"), (2, "c"), (3, "d")]

for i, buchstabe in neu:  # oder kürzer: in enumerate(liste):
    print(f"Der Buchstabe an Stelle {i} ist {buchstabe}.")

# anstatt:
for i in range(liste):
    buchstabe = liste[i]
    print(...)
    
```

List-comprehension:
```python
liste = ["a", "b", "c", "d", "e", "f", "c"]
# liste2 = ["aaa", "bbb", ...]

# Die klassische Variante, ein bisschen mühsam
liste2 = []
for c in liste:
    liste2.append(c*3)
# print(liste2)  # Zum Anschauen

# Mit list comprehension: sehr schnell.
liste3 = [c*3 for c in liste]
```
Wie funktioniert .index()?
```python
liste = ["a", "b", "c", "d", "e", "f", "c"]
liste.index("c")  # => 2
buchstabe = liste[2] # => "c"
liste[2] = "C"
liste[liste.index("c")] = "C"

```