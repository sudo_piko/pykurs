# Pikos Python Kurs am 
Python-Kurs am 5. Mai 2022
Thema: Apps auf der CARD10

# Aufgabe 1: Hatchery
- https://badge.team/ ist sozusagen der App-Store für die card10, aber auch für andere Badges
- im filter card10-badge auswählen zeigt nur die für die card10 an 
- Filter reduziert Seiten von 17 auf 4, d. h. Ratio = ca. 4/17
- Bewertung: hinten in der Tabelle mit Daumen hoch/Schweinchen/Daumen runter 
- am besten bewertet für die card10 ist das Spiel "micromarble", das Piko sehr empfehlen kann, auch weil es recht awesome ist, dass so etwas auf der card10 läuft

# Aufgabe 2: eine App anschauen und installieren

Bsp: North Sense
- im Download sind 3 Dateien enthalten: eine .py, eine .json und eine .md
- alle Dateien kann ich auch direkt in der Hatchey öffnen und sie mir anschauen bevor ich sie herunterlade
- .json ist ein Dictionary von JavaScript (json: java script object notation)
- üblich ist auch ein README in den Projekten (groß geschrieben!)
- .md - Markdown: eine Syntax, die u. a. in Markdown-Pads benutzt wird wie sie Piko auch für den Kurs benutzt
- eigentliche Installationsdatei ist die init.py

# __init__.py anschauen:

Block 1 (Z 1-6)
- imports von Funktionen der card10 für die App, zB display, buttons, vibra(tion), math (etwas muss berechnet werden)

import bhi160
import utime
import display
import vibra
import buttons
import math

Block 2 (Z 8-9)
- Variablen werden festgelegt: freq = 0.2 > Messungsfrequenz mit 0.2 sek) und fov = 45 (Grad) ("field of vision in degrees")

fov = 45  # field of vision in degrees
freq = 0.2  # how often to measure, in seconds

Block 3 (Z 11-12)
- Visualisierung wird auf dem Display platziert: center = 80px nach rechts, 40px nach unten 
- ein Radius wird fesgelegt mit 30 Grad

center = (80, 40)
radius = 30

Block 4 (Z 14-15)
- orientation = bhi160.BHI160Orientation() > ??? 
> in der Suchmaschine nachschauen, was das ist: ein Sensor, ein Akzeleromometer
- Vermutung: das Modul ist wahrscheinlich ein Modul, das auf der Card10 ist, aber kein Python-Modul > hier soll mit dem Sensor festgestellt werden, in welcher Orientierung die card10 sich im Raum befindet/wie sie gerade gehalten wird (schräg, waagericht, aufrecht, ...)
- die display-methode wird angesprochen und der display geöffnet/eingeschaltet

orientation = bhi160.BHI160Orientation()
disp = display.open()

Block 5 (Z 17-41)
- jetzt folgt eine Definition, die wir uns zunächst nicht genauer anschauen

Block 6 (Z 44-80)
- es folgt eine While-Schleife, mit der Code laufen bzw. unterbrochen werden soll

ab jetzt ist es sinnvoll, den Code von hinten anzuschauen: die Funktion in Block 5 erst anschauen an dem Punkt, wo sie eingebettet ist und benutzt wird

in der While-Schleife wird u.a.:
- ein Kompass gezeichnet (Z 61)
- geschaut, ob Knöpfe gedrückt wurden (ab Z 72)
- ein sample festgelegt:

Z 45 sample = orientation.read 
- > jetzt siehe oben, Z 14: Was ist orientation?
- weil .read ausgeführt werden soll, muss orientation ein Objekt sein, denn read ist eine Methode: Lies den Status und gib den Status zurück
- der Status beinhaltet mehrere Informationen, da "samples" im Plural steht (und dies ein gut geschriebener Code ist, dem mensch hier vertrauen kann, dass wirklich gemeint ist, was da steht)

Was ist sample?
- samples kann laut Z 46 die Länge 0 haben, könnte also eine Liste sein, die in diesem Fall leer wäre 
- Erklärung Z 48: Wenn du ein Sample zurückbekommen hast, fang ab Z 45 neu an ("continue")
- sample ist offenbar auch ein Objekt, denn in Z 52 kann "sample.x" ausgeführt werden

Welche Farben werden ab Z 53 definiert?
- Liste mit 3 Zahlen zeigt: rgb-Modus wird festgelegt
- d.h. 1. Stelle = Rotanteil, 2. Stelle Gelb, 3. Stelle blau, ergibt gemäß Mischung der Lichtfarben (additiv) rot, orange, gelb und grün

weiter im Code: Z 63
- wenn der Status 3 (entspricht "grün", also wahrscheinlich gemäß unserer farbprägung codiert als gut/grünes Licht (im Gegensatz zu rot: schlecht/Stopp) erreicht ist, dann tue folgendes: ...

----- North Sense am besten spätestens jetzt installieren, um wirklich zu sehen, was passiert: ---
kurzes How-To: App auf den Rechner runterladen, card10 per USB anschließen und im Menü auf "USB Storage" navigieren, jetzt solltest du auf die card10 vom Rechner aus zugreifen können; die North-Sense-App (entpackt!) in den card10-App-Ordner schieben, card10 auswerfen (!)
siehe auch: https://card10.badge.events.ccc.de/userguide/general-usage/#installing-apps-via-usb

ausprobieren, was passiert:
- card10 vibriert, sobald die "Nase" im Kompasskreis nach Norden zeigt
- > wenn mensch das card10 immer trägt, kann es eine Intuition entwickeln, wo Norden ist :]

zurück zu Z 63: was passiert im Code?
- wenn "pointing_north" = degrees ... :
- von hinten aufgerollt: wenn "fov/2" (45/2) oder "360 - 45/2" Grad (nach Punkt-Vor-Strichrechnung berechnet), d. h. wenn die "Nase" kleiner ist als 2 Uhr oder größer als 22 Uhr 
- wenn eins von beiden True ist, dann wird die Zeile true > sodann:
- vibra.set(pointing_north): Vibrieren wird eingeschaltet, wenn die "Nase" gen Norden zeigt
- dann wird die Hälfte der frequenz freq (siehe Z 9) gewartet ("sleep")
- dann wird Vibration wieder ausgeschaltet (= Vibrationspause)
- dann wird Vibration wieder eingeschaltet 
- > das geht solange weiter wie die "Nase" nach Norden zeigt

Z 72-80:
- prüfe, ob Knöpfe gedrückt werden
- ein (dieser) Code muss nicht völlig verstanden werden! einfach nur grob schauen, was da wohl passieren könnte:
- Z 77 und Z 78: analog zu i = i+1, was ja auch als i += 1 geschrieben werden kann, funktioniert auch Z 78:
freq *= 1.2  heißt also: freq = freq * 1.2, d. h. die Vibrationsfrequenz wird auf 1,2 erhöht (um ein Fünftel), wird also langsamer beim Drücken des Knopfes rechts unten ("BOTTOM_RIGHT")
- ebenso Z 75 und Z 76: Frequenz wird kleiner um ein Fünftel, d.h. Vibration wird schneller mit Knopf links unten ("BOTTOM_LEFT")

jetzt zurück zu Block 5, der Definition:
- Z 18: funtion draw_compass: zeichnet einfach die Kompass-Grafik mit dem Kreis, der Nase und dem Punkt, der anzeigt, wohin mensch mit der card10 zeigt

# Hausaufgabe = Aufgabe 2.2 zu heute: Code verändern
- die Farben verändern - Zeilen 53-59
- oder Radius oder Frequenzveränderung - oder was auch immer :)
- auch probieren, ob/wie die App kaputt geht - Memo: die App kann ich ja jederzeit wieder intakt neu runterladen und überspielen

