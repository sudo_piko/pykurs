# Pikos Python Kurs am 20.01.2022

## Mitschrift 
### Tipp

Schöne gutsortierte Seite mit cheatsheets: https://quickref.me/python

### Besprechen der Hausaufgaben
#### Aufgabe 1: Quiz

- Nach einem if kann man alles machen, was True oder False rauswirft, deswegen kommt das if auch mit Funktionen klar, wie
- if fragestellen(frage, quiz_daten[fragen]):
- f-string formatiert die eingabe in einen string
- anzahl_fragen = len(quiz_daten) wirft raus, wie viele Fragen-Antwort-Paare im dictionary stehen und kann als maximale Punktzahl ausgegeben werden (quiz_daten war in diesem beispiel der name des dictionarys)
- pikos geständnis: das wort "frage" als name kommt zu oft vor. einerseits ist frage ein parameter (Platzhalter) für die Funktion, dann wird der begriff für die For-Schleife benutzt "for frage", in Zeile 20 als variablennamen.
- Klassische variablendefinition: f = quiz_fragen[i]
- bei der doppelverwendung vom gleichen wort in unterschiedlichen anwendungen, beschwert sich pycharm, weils schlechter stil ist, aber nicht so schlecht, als dass es gar nicht läuft
- for-schleife ist hier outer scope = nur eine schleife, macht keinen eigenen namespace auf, im gegensatz zu einer funktion
- mehrere leerzeichen = auch schlechter stil
- pikos nächstes geständnis: fragestellen ist kein guter name für eine funktion, eher "kennt_die_richtige_antwort" weil True oder False rausfällt, macht die Zeile verständlicher (liest sich dann wie eine Anweisung)
- format-string "punkte" entspricht der variable punkte

**Besprochener Code der Gruppe:**

```quiz_daten = {"3+5":"8", "Was machen Pandas im Winter?":"Pandarolle"}

def fragestellen(frage, richtige_antwort):
    gegebene_antwort = input(frage + "")
    if gegebene_antwort == richtige_antwort:
        print("Richtig!")
        return True
    else:
        print("Falsch!")
        return False

name = input("Hallo! Willkommen zum Quiz. Wer bist Du?")

punkte = 0

# Ich gehe das Dictionary "quiz_daten" durch und schaue, ob gegebene_antwort und richtige_antwort für jede
# Frage und Antwort übereinstimmt. quiz_daten[frage]: Nimm den Value der zu einem Key "frage" im Dictionary
# "quiz_daten" passt.
for f in quiz_daten:
    if fragestellen(f, quiz_daten[f]):
        punkte += 1
    print(f"derzeitiger Punktestand: {punkte}")
# f-String: formatted string, gibt einen String aus und automatisch wird angepasst, was kein String ist und in {} steht.

anzahl_fragen = len(quiz_daten)
# len() zählt die key-value-Paare

print(f"Quiz beendet! Erreichte Punkte: {punkte} von {anzahl_fragen} möglichen Punkten.")
```


**********

### Extra-Aufgabe: Dictionaries: Histogramm

https://stackoverflow.com/questions/21195179/plot-a-histogram-from-a-dictionary

- histogramm-eintrag vom key buchstabe wird mit .get geholt, alle zeichen werden gezählt und aufgezählt
- ein import sollte immer on top of the file sein, sonst schlechter stil (;
- importiert werden: numpy as np und matplotlib.pyplot as plt
- mit "import ... as" kann man numpy zu np abkürzen, quasi einen kürzeren namen vergeben
- plt.bar(h.keys(), h.values(), width, color='g') definiert, dass ein plot mit balken generiert wird, wie die beschriftung aussieht, wie weit sie ist und welche farben grün sind

************

## Mastermind

Trennung zwischen Grafik und Programmierlogik
- Grafik in Turtle machen
- Programmlogik ist unabhängig davon
- zum nächsten Mal mit der Grafik rumspielen und in der Turtle das Design ausprobieren
- und drüber nachdenken, wie die gebrainstormten dinge eingebaut werden könnten
- hausaufgabe: mastermind spielen ^^

### Wir brainstormen, was wir für mastermind brauchen:

definition: was ist mastermind, was passiert da??? (DER docstring): rate einen mehrteiligen Farbcode. bekomme hinweise auf richtige Farben/Positionen nach jedem Raten)
die richtigen Farben: perfekte Farbnamen (oder Hex-Code für Farben)
RED, GREEN, YELLOW, BLUE, BLACK, ORANGE

Konstanten:
* Anzahl Versuche festlegen und runterzählen
* Anzahl Farben (insgesamt und im Code)
* Anzahl Slots
* nth: Auswahl: Allow Duplicates

random importieren für zufälligen Farb-Code Irgendwas, das Farb-Code am Anfang festlegt

input mit Lösungsversuch

Lösungsversuch und Lösung (= Farb-Code) vergleichen

Punkte für richtige Farbe an richtiger Stelle (im Spiel rot) und Punkte für richtige Farbe, aber an falscher Stelle (im Spiel weiß).

lass user entscheiden wie viele farben, versuche, doubles or not

error für unvollständigen guess (= nicht genug Farben im Lösungsversuch angegeben)

ausgabe punkte am ende

# Pad

https://md.ha.si/J4Bb4z7uQ9e15_MWLprDZw?edit