# Pikos Python Kurs am 24.03.2022

Weitere Mitglieder-Klassen gerne hierhin:
https://md.ha.si/10163BrWQe29zGfcJ5-19g?edit#
 

## Zwei wichtige Sätze

"adas_fahrrad ist eine Instanz der Klasse 'Fahrrad'. adas_fahrrad ist ein Objekt."
"'self' ist eigentlich ein Platzhalter für das jeweilige Objekt."

## Pikos diesmal sehr ungeordnete Datei
```python

import turtle


class Dreieck:

    def __init__(self, kantenlaenge=50, farbe='green', dicke='3', fuell_farbe=None):
        self.kantenlaenge = kantenlaenge
        self.farbe = farbe
        self.dicke = dicke
        self.gefuellt = fuell_farbe

    def malen(self):
        t = turtle.Turtle()
        t.pensize(self.dicke)
        t.pencolor(self.farbe)
        if self.gefuellt:
            t.fillcolor(self.gefuellt)
            t.begin_fill()
            t.circle(radius=self.kantenlaenge, steps=3)
            t.end_fill()
        else:
            t.circle(radius=self.kantenlaenge, steps=3)

    def werte_ausgeben(self):
        print(f'Dieses wundervolle Dreieck hat eine Kantendicke von {self.dicke} mit der Länge {self.dicke}. Gefüllt '
              f'ist es {self.gefuellt if self.gefuellt else "gar nicht"}')

    def rotate(self):
        ...


dreieck1 = Dreieck(fuell_farbe='purple')
dreieck1.malen()
dreieck1.werte_ausgeben()
turtle.exitonclick()





import turtle
from playsound import playsound
####Code angepasst und erweitert:


class Fahrrad:

    # __init__ baut uns ein neues Fahrrad-Objekt:
    def __init__(self, besitzy,farbe, reifen_groesse, kaputt=False, klingeling = 'klingel.mp3'):
        self.farbe = farbe #Attribut
        self.reifen_groesse = reifen_groesse
        self.kaputt = kaputt
        self.besitzy = besitzy
        self.klingel = klingeling

    def anmalen(self, neue_farbe='green'):
        self.farbe = neue_farbe

    def reifen_aufpumpen(self):
        self.reifen_groesse += 1

    def reifen_entlueften(self):
        self.reifen_groesse -= 1

    def kaputtmachen(self):
        self.kaputt = True

    def reparieren(self):
        self.kaputt = False

    def verschenken_an(self, beschenkte_person):
        self.besitzy = beschenkte_person

    def wert_zeigen(self):
        print(f"Das Fahrrad von {self.besitzy} ist {self.farbe} und hat die Reifengröße {self.reifen_groesse}")

    def klingeln(self):
        print("\a")
        #oder
        playsound(self.klingel)

    def malen(self):
        t = turtle.Turtle()
        t.pensize(2)
        t.pencolor(self.farbe)
        t.lt(120)
        # Hinterreifen
        t.pd()
        t.circle(self.reifen_groesse)
        t.pu()
        # Sattel und Stange
        t.lt(75)
        t.fd(10)
        t.lt(25)
        t.fd(self.reifen_groesse/2)
        t.pd()
        t.rt(45)
        t.fd(100)
        # Vorderreifen
        t.pd()
        t.circle(self.reifen_groesse)

        #Stange und Lenker
        t.rt(75)
        t.fd(40)
        t.rt(70)
        t.fd(25)

# Das lässt sich dann so aufrufen:
fahrrad1 = Fahrrad("Ada", "blue",24)
print(type(1))
print(type(fahrrad1))
fahrrad2 = Fahrrad("Margaret", "red",12)

#fahrrad1.wert_zeigen()
fahrrad1.malen()  # Das Argument "self", das oben in der Definition steht, ist "fahrrad1", und das steht dann vor dem Punkt.
fahrrad1.klingeln()
fahrrad1.anmalen('yellow')
fahrrad1.malen()
dreieck2 = Fahrrad("Paul", "magenta", 20)
dreieck2.malen()

turtle.exitonclick()

# Dreiecks-FUNKTION; in laenge wird beim Aufrufen der Funktion etwas engesetzt.
def dreieck(laenge):
    for i in range(3):
        turtle.fd(laenge)
        turtle.rt(120)

dreieck(100)

```

```python
# # Hier ein Schnipsel für eine Mitglied-Klasse:
    def name_aendern(self, name_neu=None, vorname_neu=None):  # Happy Trans-Visibility-Day übrigens!
        if not name_neu:          # Das hier ist nicht das, was wir wollen. Analogon zur Dreieck-Klasse, aber bei
            self.name = name_neu  # der Mitgliederdatenbank wollen wir es ja genau andersrum.
        if vorname_neu:  # Hier stimmt es!
            self.vorname = vorname_neu
mitglied1.name_aendern(name_neu="Meier")

# analog zu Dreieck
    def (self, farbe=None, fuellfarbe=None):
        if not farbe:  # Falsy  "falschig", Truthy "wahrig"
            farbe = self.farbe
        if not fuellfarbe:
            fuellfarbe = self.fuellfarbe

```

```python

"""adas_fahrrad ist eine Instanz der Klasse 'Fahrrad'. adas_fahrrad ist ein Objekt."""
"""'self' ist eigentlich ein Platzhalter für das jeweilige Objekt."""

fahrradkeller = [fahrrad1, fahrrad2, dreieck2]
farbenliste = ["red", "green", "blue"]

for farbe in farbenliste:
    print(farbe)

for fahrrad in fahrradkeller:
    fahrrad.malen()

fahrrad = fahrrad1
fahrrad.malen()
fahrrad = fahrrad2
fahrrad.malen()


# Antwort auf Frage

aenderungsliste = [mitglied1, ["Julika", "von Schattenfels", "Schlossallee 1", "86831 Haunstetten"],
                   mitglied1, ["Julika", "von Schattenfels", "Schlossallee 1", "86831 Haunstetten"],
                   mitglied1, ["Julika", "von Schattenfels", "Schlossallee 1", "86831 Haunstetten"] 
                   ]  # lange liste mit Mitgliedern und Änderungen

def mitgliederaendern(mitglied, neue_daten):
    vorname_neu, nachname_neu, strasse_neu, ort_neu = neue_daten
    mitglied.name_aendern(vorname_neu, nachname_neu)
    mitglied.adresse_aendern
    ...

for mitglied, neue_daten in aenderungsliste:
    mitgliederaendern(mitglied, neue_daten)
```