# Pikos Python Kurs am 10.02.2022

## Mitschrift 

## Pikos Datei


```
r r r g   LÖSUNG
r r r x   LÖSUNG HELPER 

loesung_helper = loesung[:] – Notwendig wegen Listenkopieren...

r g b l		VERSUCH

g g g g		VERSUCH


Unser Algorithmus:
1 Was ist richtig und am richtigen Ort
2 => 1 richtig-Pin
3 Kommt SONST noch was vor? Ignoriere die, die in schritt 1 schon dran waren!
4 => 1 fast-richtig-Pin



PSEUDOCODE:
solange noch rateversuche übrig:
lösungshelper = kopie von lösung
for indexzahl in Länge von Code do:
	wenn pinfarbe am Index = pinfarbe von Lösung am Index:
		richtig + 1
		Lösung am Index = X
```
```

a = 1
b = a
c = 1
for n in (a,b,c):
	print(n)  # Alle 1

for n in (a,b,c):
	print(id(n))  # Alle haben die gleiche ID

b += 1
for n in (a,b,c):
	print(id(n))  # b ist jetzt 2, a und c sind noch 1. b hat eine neue ID.

=> NEUES objekt.

genauso mit strings!


ABER ANDERS BEI LISTEN:
>>> a = [1,2,3]
>>> b = a
>>> c = [1,2,3]
>>> for n in (a,b,c):
...     print(n)
... 
[1, 2, 3]
[1, 2, 3]
[1, 2, 3]
>>> for n in (a,b,c):
...     print(id(n))
... 
140290965739584
140290965739584  # b hat die gleiche ID wie a
140290965656512  # c hat eine andere! Ist woanders gespeichert



>>> a += [4,5]   # Wir verändern nur a
>>> for n in (a,b,c):
...     print(n)
... 
[1, 2, 3, 4, 5]   # wir haben a verändert
[1, 2, 3, 4, 5]   # aber b hat sich auch verändert!
[1, 2, 3]



for n in (a,b,c):
	print(id(n))    # Wir haben das Dings an der ID von a geändert...

		
```