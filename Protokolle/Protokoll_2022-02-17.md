# Pikos Python Kurs am 10.02.2022

## Mitschrift 

# Pads
Von den letzten Malen:  
https://md.ha.si/cMGZMFhDSNqmVxsTNvnBww#  
https://md.ha.si/VTjPab4PSQyUvat8TnLXBw#

Von diesem Mal:  (Ganz unten)
https://md.ha.si/uSTHY1VhS12M60UKxi8rpA?edit

## Pikos Datei

break
```python
a = 2

while True:
    print(a)
    a += 1
    if zimmer == 4:
        break

```

Methoden vs. Funktionen vs. importierte Funktionen
```python
# Methoden hängen immer mit Punkten an Objekten:

liste = [1, 2, 3]  # <- Objekt
liste.append(4)  # <- Methode

from time import sleep
sleep(4)  # <- Funktion

import time
time.sleep(3)  # <- auch Funktion

print("Hallo") # <- auch Funktion, die zu Pythons "normalen" Funktionen gehört. ("liegt im globalen Namespace")
```

Methoden sind ein Spezialfall von Funktionen: 
Methoden sind Funktionen, die an einem Objekt hängen müssen.
```python
import turtle

hint_turtle = turtle.Turtle()  # <- ein Objekt!

hint_turtle.fd(100)  # <- Methode, weil hängt an einem Objekt.
hint_turtle.bye()  # <- keine Methode => Fehlermeldung
turtle.bye()  # <- eine Funktion!

```


## Mastermindproblem

Algorithmus:
`    # if color anwesend
    # gib    weiss
    # if color in right place
    # take    weiss    gib    rot
    # else
    # gib    nix

    
    lösung: gggr, versuch: rrrr – was passiert dann?
    lösung: blgr, versuch: rrrr – was passiert dann?