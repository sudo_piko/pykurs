# Pikos Python Kurs am 16.06.2022
Letzte Stunde.

Vielen Dank an Euch alle, es hat wahnsinnig Spaß gemacht mit Euch neugierigen und motivierten Leuten! 
Liebe Grüße an alle, die durchgehalten haben und an alle, die aus welchen Gründen auch immer während des Jahres verloren 
gegangen sind. Ich wünsche Euch viel Spaß und Mut im Umgang mit Technik und mit dem Programmieren!

## Pads
Pad mit Ideen für die Card10:
https://md.ha.si/KyNpRRqCRnK3866EroL9bg?both

## Spacepaint
https://hatchery.badge.team/projects/spacepaint

## Wie jetzt weiter?
- https://www.reddit.com/r/ProgrammerHumor/comments/v9tsrp/im_both/
- bleibt in Eurer Gruppe!
- Sucht Euch Projekte:
  - Project Euler für Mathe-Liebhabende: https://projecteuler.net/
  - Advent of code (Am besten auch in Eurer Gruppe): https://adventofcode.com/
  - Automate the boring stuff with python: http://automatetheboringstuff.com/
  - Alle Aufgaben hier im Gitlab! Habt Ihr wirklich *jede* Hausaufgabe gelöst?
- Pykurs 2: Bald™ gibt es dazu eine Ankündigung auf der Haecksen-Mailingliste.

## Projekteliste
Diese Liste hatten wir mal gesammelt:
- Pong: Hierfür findet sich eine Anleitung unter https://jugendhackt.org/oer/projekte/python-einfuehrung/
- Hangman: Das schafft Ihr direkt!
- Snake: Da müsst Ihr ein bisschen bei der Pong-Anleitung klauen.
- Todo-Liste: Packt Eure Todo-Liste in ein Hedgedoc und findet heraus, was folgendes tut: 
    ```
  fid = urllib.request.urlopen(url)
  webpage = fid.read().decode('utf-8') ```
- Tetris: Auch da könnt Ihr bei Pong klauen oder es an  
- tic tac toe 2: Das kriegt Ihr direkt hin! Das Spielfeld ist eine Liste mit drei Listen, die jeweils drei Felder als Element haben.
- wordle: Das kriegt Ihr auch hin.

Nehmt Euch gerne Projekte daraus vor! Ich helfe auch gerne bei Fragen.
