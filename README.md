# Python-Kurs Haecksen

Hier finden sich Aufgaben, Hinweise und Materialien für den ersten Pythonkurs für absolute Anfängerinnen*, aka "Abraxas".

Der Kurs lief von August 2021 bis Juni 2022.

# Nachfolger ab August 2022

Ab August 2022 läuft ein neuer Kurs. Materialien finden sich hier:  
https://gitlab.com/sudo_piko/pythonkurs-b


# Projekt-Ideen für den Abraxas-Kurs
Hier werden noch einige Projekt-Ideen gesammelt.

## Random Quiz und Multiclipboard
Auf dieser Seite finden sich zwei großartige Projekte, allgemein ist Atomatetheboringstuff sehr empfehlenswert:  
http://automatetheboringstuff.com/2e/chapter9/
